export function handler(event: any, _: any, callback: any) {
  callback(null, {
    statusCode: 200,
    body: JSON.stringify({ msg: 'Hello, World!' }),
  });
}

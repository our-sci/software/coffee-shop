import { useCallback } from 'react';
import { atom, useRecoilValue, useSetRecoilState } from 'recoil';

const highlightedFilterIdAtom = atom<null | string>({
  key: 'highlighted-filter-id',
  default: null,
});

export const useHighlightedFilterId = () => useRecoilValue(highlightedFilterIdAtom);

export const useHighlightFilter = () => {
  const set = useSetRecoilState(highlightedFilterIdAtom);
  return useCallback((filterId: string) => set(filterId), [set]);
};

export const useUnhighlightFilter = () => {
  const set = useSetRecoilState(highlightedFilterIdAtom);
  return useCallback(
    (filterId: string) => set((current) => (current === filterId ? null : current)),
    [set],
  );
};

import { useCallback } from 'react';
import { atom, useRecoilValue, useSetRecoilState } from 'recoil';

const highlightedPlantingIdAtom = atom<null | string>({
  key: 'highlighted-planting-id',
  default: null,
});

export const useHighlightedPlantingId = () => useRecoilValue(highlightedPlantingIdAtom);

export const useHighlightPlanting = () => {
  const set = useSetRecoilState(highlightedPlantingIdAtom);
  return useCallback((plantingId: string) => set(plantingId), [set]);
};

export const useUnhighlightPlanting = () => {
  const set = useSetRecoilState(highlightedPlantingIdAtom);
  return useCallback(() => set(null), [set]);
};

import { useCallback } from 'react';
import { atom, useRecoilValue, useSetRecoilState } from 'recoil';

const pinnedPlantingIdsAtom = atom<string[]>({
  key: 'pinned-planting-id',
  default: [],
});

export const usePinnedPlantingIds = () => useRecoilValue(pinnedPlantingIdsAtom);

export const usePinPlanting = () => {
  const set = useSetRecoilState(pinnedPlantingIdsAtom);
  return useCallback(
    (plantingId: string) =>
      set((prev) =>
        [...prev, plantingId].filter((item, index, ary) => ary.indexOf(item) === index),
      ),
    [set],
  );
};

export const useUnpinPlanting = () => {
  const set = useSetRecoilState(pinnedPlantingIdsAtom);
  return useCallback(
    (plantingId: string) => set((prev) => prev.filter((item) => item !== plantingId)),
    [set],
  );
};

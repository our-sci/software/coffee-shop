import { useCallback } from 'react';
import uniqueId from 'lodash/uniqueId';
import { Notification, NotificationTypeEnum } from 'models/Notification';
import { atom, useRecoilValue, useSetRecoilState } from 'recoil';
import { setRecoil } from 'recoil-nexus';

const isAuthDialogOpenAtom = atom<boolean>({
  key: 'is-auth-dialog-open',
  default: false,
});

export const useIsAuthDialogOpen = () => useRecoilValue(isAuthDialogOpenAtom);
export const useSetIsAuthDialogOpen = () => useSetRecoilState(isAuthDialogOpenAtom);

const isCropSelectorDialogOpenAtom = atom<boolean>({
  key: 'is-crop-selector-dialog-open',
  default: false,
});

export const useIsCropSelectorDialogOpen = () => useRecoilValue(isCropSelectorDialogOpenAtom);
export const useSetIsCropSelectorDialogOpen = () => useSetRecoilState(isCropSelectorDialogOpenAtom);

const notificationsAtom = atom<Notification[]>({
  key: 'notifications',
  default: [],
});

export const useNotifications = () => useRecoilValue(notificationsAtom);

export const addErrorNotification = ({ message }: { message: string }) => {
  setRecoil(notificationsAtom, (state) => [
    ...state,
    { type: NotificationTypeEnum.ERROR, message, id: uniqueId() },
  ]);
};

export const useRemoveNotification = () => {
  const set = useSetRecoilState(notificationsAtom);
  return useCallback((id: string) => set((state) => state.filter((n) => n.id !== id)), [set]);
};
export const useRemoveAllNotifications = () => {
  const set = useSetRecoilState(notificationsAtom);
  return useCallback(() => set([]), [set]);
};

import { useCallback } from 'react';
import { atom, useRecoilValue, useSetRecoilState } from 'recoil';

type SidePanelContent = {
  plantingIds: string[];
} & (
  | {
      type: 'FilterEditor';
      filterId: string;
    }
  | {
      type: 'PlantingCards';
    }
  | {
      type: 'Profile';
      producerId: string;
    }
);

const sidePanelContentAtom = atom<SidePanelContent>({
  key: 'side-panel-content',
  default: {
    type: 'PlantingCards',
    plantingIds: [],
  },
});

export const useSidePanelContent = () => useRecoilValue(sidePanelContentAtom);
export const useShowProfile = () => {
  const set = useSetRecoilState(sidePanelContentAtom);
  return useCallback(
    (producerId: string) => set((state) => ({ ...state, type: 'Profile', producerId })),
    [set],
  );
};
export const useShowFilterEditor = () => {
  const set = useSetRecoilState(sidePanelContentAtom);
  return useCallback(
    (filterId: string) => set((state) => ({ ...state, type: 'FilterEditor', filterId })),
    [set],
  );
};
export const useShowPlantingCards = () => {
  const set = useSetRecoilState(sidePanelContentAtom);
  return useCallback(() => set((state) => ({ ...state, type: 'PlantingCards' })), [set]);
};
export const useAddPlantingCard = () => {
  const set = useSetRecoilState(sidePanelContentAtom);
  return useCallback(
    (plantingId) =>
      set((state) => {
        if (!state.plantingIds.includes(plantingId)) {
          return { ...state, plantingIds: [plantingId, ...state.plantingIds] };
        }
        return state;
      }),

    [set],
  );
};
export const useRemovePlantingCard = () => {
  const set = useSetRecoilState(sidePanelContentAtom);
  return useCallback(
    (plantingId) =>
      set((state) => {
        if (state.plantingIds.includes(plantingId)) {
          return {
            ...state,
            plantingIds: state.plantingIds.filter((id) => id !== plantingId),
          };
        }
        return state;
      }),
    [set],
  );
};

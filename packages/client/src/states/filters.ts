import { useCallback } from 'react';
import { schemeTableau10 } from 'd3-scale-chromatic';
import sample from 'lodash/sample';
import without from 'lodash/without';
import { Filter, FilterParam, FilterParamDataSource, FilterValue } from 'models/Filter';
import { atom, useRecoilValue, useResetRecoilState, useSetRecoilState } from 'recoil';

const filtersAtom = atom<Filter[]>({
  key: 'filters',
  default: [],
});

export const useFilters = () => useRecoilValue(filtersAtom);

export const useFindFilter = () => {
  const filters = useRecoilValue(filtersAtom);
  return {
    findFilterById: useCallback(
      (id: string): Filter | undefined => filters.find((filter) => filter.id === id),
      [filters],
    ),
    findFilter: useCallback(
      (filter: Partial<Filter>): Filter | undefined => {
        const keys = Object.keys(filter);
        return filters.find((item) =>
          keys.every(
            (key) =>
              JSON.stringify(item[key as keyof Filter]) ===
              JSON.stringify(filter[key as keyof Filter]),
          ),
        );
      },
      [filters],
    ),
  };
};

export const useUpdateFilterName = () => {
  const set = useSetRecoilState(filtersAtom);
  return useCallback(
    (filterId: string, name: string) =>
      set((filters) => filters.map((f) => (f.id === filterId ? { ...f, name } : f))),
    [set],
  );
};

let filterId = 0;
let filterNamePostfix = 1;
const COLORS = schemeTableau10.slice(0, 9);
export const useAddFilter = () => {
  const set = useSetRecoilState(filtersAtom);
  return useCallback(
    (filter: Partial<Filter> = {}) => {
      const id = (filterId++).toString();
      set((filters) => {
        const name = `New Filter ${filterNamePostfix++}`;
        const freeColors = without(COLORS, ...filters.map((g) => g.color));
        const color = sample(freeColors.length > 0 ? freeColors : COLORS)!;
        const newFilter: Filter = {
          id,
          name,
          color,
          params: [],
          ...filter,
        };
        return [...filters, newFilter];
      });
      return id;
    },
    [set],
  );
};

export const useRemoveFilter = () => {
  const set = useSetRecoilState(filtersAtom);
  return useCallback(
    (filterId: string) => set((filters) => filters.filter((f) => f.id !== filterId)),
    [set],
  );
};

export const useResetFilter = () => {
  const set = useResetRecoilState(filtersAtom);
  return useCallback(() => set(), [set]);
};

export const useEditFilterParam = () => {
  const set = useSetRecoilState(filtersAtom);
  return useCallback(
    (filterId: string, key: string, value: FilterValue) =>
      set((filters) =>
        filters.map((f) =>
          f.id === filterId
            ? {
                ...f,
                params: f.params.map((p) => (p.key === key ? { ...p, value } : p)),
              }
            : f,
        ),
      ),
    [set],
  );
};

export const useSetFilterParamActive = () => {
  const set = useSetRecoilState(filtersAtom);
  return useCallback(
    (filterId: string, key: string, active: boolean) =>
      set((filters) =>
        filters.map((f) =>
          f.id === filterId
            ? {
                ...f,
                params: f.params.map((p) => (p.key === key ? { ...p, active } : p)),
              }
            : f,
        ),
      ),
    [set],
  );
};

export const useAddFilterParam = () => {
  const set = useSetRecoilState(filtersAtom);
  return useCallback(
    (filterId: string, param: FilterParam) =>
      set((filters) =>
        filters.map((f) =>
          f.id === filterId
            ? {
                ...f,
                params: [...f.params.filter((p) => p.key !== param.key), param],
              }
            : f,
        ),
      ),
    [set],
  );
};

export const useRemoveFilterParam = () => {
  const set = useSetRecoilState(filtersAtom);
  return useCallback(
    (filterId: string, key: string) =>
      set((filters) =>
        filters.map((f) =>
          f.id === filterId
            ? {
                ...f,
                params: f.params.filter((p) => p.key !== key),
              }
            : f,
        ),
      ),
    [set],
  );
};

export const createOptionFilterParam = (key: string, options: string[]) => ({
  active: true,
  dataSource: FilterParamDataSource.FarmOnboarding,
  key,
  value: { options },
});

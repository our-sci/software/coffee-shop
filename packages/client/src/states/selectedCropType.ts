import { atom, useRecoilValue, useSetRecoilState } from 'recoil';

const selectedCropTypeAtom = atom<string>({
  key: 'selected-crop-type',
  default: '',
});

export const useSelectedCropType = () => useRecoilValue(selectedCropTypeAtom);
export const useSetSelectedCropType = () => useSetRecoilState(selectedCropTypeAtom);

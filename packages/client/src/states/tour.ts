import { useCallback } from 'react';
import { atom, useRecoilValue, useSetRecoilState } from 'recoil';
import { z } from 'zod';

const STORE_KEY = 'coffeeshop.tour';

export enum StopEnum {
  SELECT_CROP,
  FILTER,
  HOVER_VALUE,
  OPEN_PLANTING,
  DISCUSS,
}

export const Stops = [
  StopEnum.SELECT_CROP,
  StopEnum.FILTER,
  StopEnum.HOVER_VALUE,
  StopEnum.OPEN_PLANTING,
  StopEnum.DISCUSS,
];

const StoreSchema = z.object({
  isTourOn: z.boolean(),
  currentStop: z.nativeEnum(StopEnum),
});

export const readFromStore = () => {
  if (localStorage[STORE_KEY]) {
    try {
      return StoreSchema.parse(JSON.parse(localStorage[STORE_KEY]));
    } catch (e) {
      console.warn('Failed to parse tour data', localStorage[STORE_KEY], e);
    }
  }
  return {
    currentStop: Stops[0],
    isTourOn: true,
  };
};

const writeToStore = (update: Partial<z.infer<typeof StoreSchema>>) => {
  localStorage[STORE_KEY] = JSON.stringify({
    ...(readFromStore() || {}),
    ...update,
  });
};

const currentStopAtom = atom<StopEnum>({
  key: 'tour.current-stop',
  default: readFromStore()?.currentStop || StopEnum.SELECT_CROP,
});

const isTourOnAtom = atom<boolean>({
  key: 'tour.is-on',
  default: readFromStore()?.isTourOn ?? true,
});

export const useCurrentStop = () => useRecoilValue(currentStopAtom);
export const useSetCurrentStop = () => {
  const set = useSetRecoilState(currentStopAtom);
  return useCallback(
    (currentStop: StopEnum) => {
      set(currentStop);
      writeToStore({ currentStop });
    },
    [set],
  );
};

export const useIsTourOn = () => useRecoilValue(isTourOnAtom);
export const useSetIsTourOn = () => {
  const set = useSetRecoilState(isTourOnAtom);
  return useCallback(
    (isTourOn: boolean) => {
      set(isTourOn);
      writeToStore({ isTourOn });
    },
    [set],
  );
};

const useStep = () => {
  const setCurrentStop = useSetCurrentStop();
  const setIsTourOn = useSetIsTourOn();
  const currentStop = useCurrentStop();
  return useCallback(
    (step: number) => {
      const currentIndex = Stops.indexOf(currentStop);
      const index = Math.max(0, currentIndex + step);
      if (index >= Stops.length) {
        setCurrentStop(Stops[0]);
        setIsTourOn(false);
      } else {
        setCurrentStop(Stops[index]);
      }
    },
    [currentStop, setCurrentStop, setIsTourOn],
  );
};
export const useNext = () => {
  const step = useStep();
  return () => step(1);
};
export const useBack = () => {
  const step = useStep();
  return () => step(-1);
};

export const useStartTour = () => {
  const setCurrentStop = useSetCurrentStop();
  const setIsTourOn = useSetIsTourOn();
  return useCallback(() => {
    setCurrentStop(Stops[0]);
    setIsTourOn(true);
  }, [setCurrentStop, setIsTourOn]);
};

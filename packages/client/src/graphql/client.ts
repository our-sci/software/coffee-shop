import { ApolloClient, ApolloLink, HttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { onError } from '@apollo/client/link/error';
import { readUserFromStore } from 'states/auth';
import { addErrorNotification } from 'states/ui';
import './server';

const authLink = setContext((_, { headers }) => {
  const user = readUserFromStore();
  return {
    headers: {
      ...headers,
      authorization: user ? `${user.email} ${user.token}` : '',
    },
  };
});

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  defaultOptions: {
    query: {
      errorPolicy: 'all',
    },
    watchQuery: {
      errorPolicy: 'all',
    },
  },
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (networkError) {
        console.warn(`[Network error]: ${networkError}`);
      }

      if (graphQLErrors) {
        graphQLErrors.forEach(({ message, locations, path }) => {
          console.error(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
          );
          addErrorNotification({
            message: `[GraphQL error]: ${message}, Path: ${path}`,
          });
        });
      }
    }),
    authLink,
    new HttpLink({ uri: '/graphql' }),
  ]),
  connectToDevTools: true,
});

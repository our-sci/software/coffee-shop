import intersection from 'lodash/intersection';
import {
  Filter,
  FilterParam,
  FilterParamDataSource,
  isOptionFilterParam,
  isRangeFilterParam,
} from 'models/Filter';
import { Planting } from 'models/Planting';

const isMatchingFarmOnboardingValue = (planting: Planting, param: FilterParam): boolean => {
  // values of this planting
  const values: string[] = [];
  if (param.key === 'organization') {
    if (planting.organization) {
      values.push(...planting.organization);
    }
  } else if (param.key === 'farmDomain') {
    if (planting.producer?.id) {
      values.push(planting.producer.id);
    }
  } else {
    const match = planting.farmOnboarding?.values?.find((v) => v.key === param.key)?.values;
    if (match) {
      values.push(...match);
    }
  }

  if (isOptionFilterParam(param)) {
    return param.value.options.some((option) => values.includes(option));
  }
  if (isRangeFilterParam(param)) {
    const numValues = values.map((v) => Number.parseFloat(v)).filter((v) => Number.isFinite(v));
    // don't filter if the planting has no value for this property
    return (
      numValues.length === 0 || numValues.some((v) => v >= param.value.min && v <= param.value.max)
    );
  }

  return false;
};

// Matching rules:
// - Range values match if
//   - planting has any value (with the given key) in the given range
//   - planting has no value with the given key
// - Option values match
//   - if the planting has a matching value (with the given key) with the selected option
export const getPlantingIdsOfFilter = (filter: Filter, plantings: Planting[]): string[] => {
  const activeParams = filter.params.filter((p) => p.active);
  if (activeParams.length === 0) {
    return [];
  }

  if (process.env.NODE_ENV !== 'production') {
    console.time(`getPlantingsOfFilter ${filter.id}`);
  }

  const filteredPlantings = intersection(
    ...activeParams.map((param) => {
      if (param.dataSource === FilterParamDataSource.FarmOnboarding) {
        return plantings.filter((planting) => isMatchingFarmOnboardingValue(planting, param));
      }
      if (isRangeFilterParam(param)) {
        return plantings.filter((planting) => {
          const values = planting.values.filter((value) => value.name === param.key);
          return (
            values.length === 0 ||
            values.some(({ value }) => value >= param.value.min && value <= param.value.max)
          );
        });
      }
      return [];
    }),
  );

  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`getPlantingsOfFilter ${filter.id}`);
  }

  return filteredPlantings.map((p) => p.id);
};

import toString from 'lodash/toString';
import uniqueId from 'lodash/uniqueId';
import { Filter, FilterParam, FilterParamDataSource, FilterValue } from '../models/Filter';
import { Planting } from '../models/Planting';
import { getPlantingIdsOfFilter } from './plantingsOfFilter';

const createFilter = (params: FilterParam[] = []): Filter => ({
  id: uniqueId(),
  name: 'foo',
  color: 'tomato',
  params,
});

const createFilterParam = (
  key: string,
  value: FilterValue,
  dataSource: FilterParamDataSource,
): FilterParam => ({
  active: true,
  dataSource,
  key,
  value,
});

const createPlanting = (
  values: Planting['values'],
  onboardingValues: NonNullable<Planting['farmOnboarding']>['values'] = [],
  organization: Planting['organization'] = [],
): Planting => ({
  id: uniqueId(),
  organization,
  values,
  farmOnboarding: {
    values: onboardingValues,
  },
});

const createValue = (name: string, value: number): Planting['values'][number] => ({
  name,
  value,
});

const createOnboardingValue = (
  key: string,
  values: string[],
): NonNullable<NonNullable<Planting['farmOnboarding']>['values']>[number] => ({
  key,
  values,
});

describe('getPlantingIdsOfFilter', () => {
  it('returns empty array when filter has no params', () => {
    const filter = createFilter([]);
    const planting = createPlanting([createValue('foo', -1)]);
    const plantingIds = getPlantingIdsOfFilter(filter, [planting]);
    expect(plantingIds).toEqual([]);
  });

  describe('range values', () => {
    [FilterParamDataSource.Values, FilterParamDataSource.FarmOnboarding].forEach((dataSource) => {
      // Create a planting with the current data source
      const createPlantingWithValue = (key: string, value: number) =>
        dataSource === FilterParamDataSource.Values
          ? createPlanting([createValue(key, value)])
          : createPlanting([], [createOnboardingValue(key, [toString(value)])]);

      describe(`data source = ${FilterParamDataSource[dataSource]}`, () => {
        it('matches if planting has no value with key', () => {
          const filter = createFilter([createFilterParam('foo', { min: 0, max: 1 }, dataSource)]);
          const planting = createPlantingWithValue('not-matching', -1);
          const plantingIds = getPlantingIdsOfFilter(filter, [planting]);
          expect(plantingIds).toEqual([planting.id]);
        });

        it('matches if planting value is in range', () => {
          const filter = createFilter([createFilterParam('foo', { min: 0, max: 1 }, dataSource)]);
          const planting = createPlantingWithValue('foo', 0.5);
          const plantingIds = getPlantingIdsOfFilter(filter, [planting]);
          expect(plantingIds).toEqual([planting.id]);
        });

        it("doesn't match if planting value is out of range", () => {
          const filter = createFilter([createFilterParam('foo', { min: 0, max: 1 }, dataSource)]);
          const planting = createPlantingWithValue('foo', 1.5);
          const plantingIds = getPlantingIdsOfFilter(filter, [planting]);
          expect(plantingIds).toEqual([]);
        });
      });
    });
  });

  describe('option values', () => {
    const dataSource = FilterParamDataSource.FarmOnboarding;

    it("doesn't match if planting doesn't have any option with the same key", () => {
      const filter = createFilter([createFilterParam('foo', { options: ['bar'] }, dataSource)]);
      const planting = createPlanting([], [createOnboardingValue('not-matching', [])]);
      const plantingIds = getPlantingIdsOfFilter(filter, [planting]);
      expect(plantingIds).toEqual([]);
    });

    it("doesn't match if planting doesn't have matching options", () => {
      const filter = createFilter([
        createFilterParam('foo', { options: ['bar', 'baz'] }, dataSource),
      ]);
      const planting = createPlanting([], [createOnboardingValue('foo', ['qux', 'zap'])]);
      const plantingIds = getPlantingIdsOfFilter(filter, [planting]);
      expect(plantingIds).toEqual([]);
    });

    it('does matches if planting has matching option', () => {
      const filter = createFilter([
        createFilterParam('foo', { options: ['bar', 'baz'] }, dataSource),
      ]);
      const planting = createPlanting([], [createOnboardingValue('foo', ['baz', 'zap'])]);
      const plantingIds = getPlantingIdsOfFilter(filter, [planting]);
      expect(plantingIds).toEqual([planting.id]);
    });
  });
});

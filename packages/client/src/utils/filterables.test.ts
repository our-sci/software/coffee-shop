/* eslint-disable jest/valid-title */
import uniqueId from 'lodash/uniqueId';
import { FilterEditorQuery } from '../components/filterEditor/FilterEditor.generated';
import {
  Filterable,
  FilterableNumeric,
  FilterableOption,
  FilterParamDataSource,
} from '../models/Filter';
import { BLACKLIST, FORCE_OPTION_TYPE, getFilterables } from './filterables';

type Planting = FilterEditorQuery['plantings'][number];
const createPlanting = (
  values: Planting['values'],
  onboardingValues: NonNullable<Planting['farmOnboarding']>['values'] = [],
): Planting => ({
  id: uniqueId(),
  values,
  farmOnboarding: {
    id: uniqueId(),
    values: onboardingValues,
  },
});

const createValue = (name: string, value: number): Planting['values'][number] => ({
  name,
  value,
});

const createValues = (name: string, values: number[]) => values.map((v) => createValue(name, v));

const createOnboardingValue = (
  key: string,
  values: string[],
): NonNullable<NonNullable<Planting['farmOnboarding']>['values']>[number] => ({
  key,
  values,
});

const createNumericFilterable = (
  key: string,
  values: number[],
  dataSource = FilterParamDataSource.Values,
): FilterableNumeric => ({
  type: 'numeric',
  key,
  values,
  dataSource,
});

const createOptionFilterable = (
  key: string,
  options: FilterableOption['options'],
): FilterableOption => ({
  type: 'option',
  key,
  options,
  dataSource: FilterParamDataSource.FarmOnboarding,
});

const runTest = (info: {
  description: string;
  plantings: Planting[];
  filterables: Filterable[];
}) => {
  it(info.description, () => {
    expect(getFilterables(info.plantings)).toEqual(info.filterables);
  });
};

describe('getFilterables', () => {
  describe('Needs at least two unique numeric values', () => {
    [
      {
        description: 'values = []',
        plantings: [createPlanting(createValues('foo', []))],
        filterables: [],
      },
      {
        description: 'values = [1]',
        plantings: [createPlanting(createValues('foo', [1]))],
        filterables: [],
      },
      {
        description: 'values = [1,2]',
        plantings: [createPlanting(createValues('foo', [1, 2]))],
        filterables: [],
      },
      {
        description: 'values = [1,2,1]',
        plantings: [createPlanting(createValues('foo', [1, 2, 1]))],
        filterables: [],
      },
    ].forEach(runTest);
  });

  [
    {
      description: 'Creates numeric filterable',
      plantings: [createPlanting(createValues('foo', [1, 2, 3]))],
      filterables: [createNumericFilterable('foo', [1, 2, 3])],
    },
    {
      description: 'Extracts numeric values from onboarding survey',
      plantings: [createPlanting([], [createOnboardingValue('foo', ['1', '2', '3'])])],
      filterables: [
        createNumericFilterable('foo', [1, 2, 3], FilterParamDataSource.FarmOnboarding),
      ],
    },
    {
      description: 'Creates option filterable',
      plantings: [createPlanting([], [createOnboardingValue('foo', ['bax', 'bax', 'baz', 'qux'])])],
      filterables: [
        createOptionFilterable('foo', [
          { value: 'bax', occurrences: 2 },
          { value: 'baz', occurrences: 1 },
          { value: 'qux', occurrences: 1 },
        ]),
      ],
    },
    {
      description: 'Accepts single option values',
      plantings: [createPlanting([], [createOnboardingValue('foo', ['baz', 'baz'])])],
      filterables: [createOptionFilterable('foo', [{ value: 'baz', occurrences: 2 }])],
    },
  ].forEach(runTest);

  describe('Hides blacklisted values', () => {
    BLACKLIST.map((key) => ({
      description: key,
      plantings: [
        createPlanting([], [createOnboardingValue('foo', ['1', '2', '3'])]),
        createPlanting([], [createOnboardingValue(key, ['1', '2', '3'])]),
      ],
      filterables: [
        createNumericFilterable('foo', [1, 2, 3], FilterParamDataSource.FarmOnboarding),
      ],
    })).forEach(runTest);
  });

  describe("Can force option type on 'numeric-looking' option values", () => {
    FORCE_OPTION_TYPE.map((key) => ({
      description: key,
      plantings: [createPlanting([], [createOnboardingValue(key, ['1', '2', '3', '1'])])],
      filterables: [
        createOptionFilterable(key, [
          { value: '1', occurrences: 2 },
          { value: '2', occurrences: 1 },
          { value: '3', occurrences: 1 },
        ]),
      ],
    })).forEach(runTest);
  });
});

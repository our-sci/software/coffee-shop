import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@emotion/react';
import * as Sentry from '@sentry/react';
import { BrowserTracing } from '@sentry/tracing';
import { theme } from './theme/theme';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './index.css';

Sentry.init({
  dsn: 'https://79a7cffafe3549c99c44ec4e9f6d7ff9@o1138516.ingest.sentry.io/6751214',
  integrations: [new BrowserTracing()],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}></ThemeProvider>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

// remove initial loading screen (added in the index.html)
try {
  // @ts-ignore
  window.loading_screen.finish();
} catch (e) {
  console.error('Failed to remove loading screen', e);
}

import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import { css, Global } from '@emotion/react';
import { App } from './components/Dashboard';
import { theme } from './theme/theme';

function Main() {
  return (
    <>
      <Global
        styles={css`
          body {
            padding: 0 !important;
          }
        `}
      />
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App iframeSrc="https://www.hylo.com/all" />
      </ThemeProvider>
    </>
  );
}

export default Main;

import { withTheme } from '@emotion/react';
import styled from '@emotion/styled';

export const Spacer = withTheme(styled.div`
  flex-grow: 1;
`);

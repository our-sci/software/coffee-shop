import { z } from 'zod';

export const loginFormSchema = z.object({
  email: z.string().min(1, 'Please input an email address').email('Invalid email address'),
  password: z.string().min(1, 'Please input a password'),
});

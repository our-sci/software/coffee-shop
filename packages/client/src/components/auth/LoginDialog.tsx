import React, { FormEvent, FunctionComponent, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import MarkunreadMailboxIcon from '@mui/icons-material/MarkunreadMailbox';
import LoadingButton from '@mui/lab/LoadingButton';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { zodResolver } from '@hookform/resolvers/zod';
import { useAuth, useLogin } from 'states/auth';
import { useIsAuthDialogOpen, useSetIsAuthDialogOpen } from 'states/ui';
import { useRequestMagicLoginLinkMutation } from './LoginDialog.generated';
import { loginFormSchema } from './LoginDialog.schema';

enum AuthMethodEnum {
  Password,
  MagicLink,
}

interface LoginFormValues {
  email: string;
  password: string;
}

interface Props {
  children?: never;
}

export const LoginDialog: FunctionComponent<Props> = () => {
  const [authMethod, setAuthMethod] = useState<AuthMethodEnum>(AuthMethodEnum.Password);
  const [showMagicLinkSent, setShowMagicLinkSent] = useState<boolean>(false);
  const [recipientEmail, setRecipientEmail] = useState<string>('');
  const { login, isLoginInProgress } = useLogin();
  const [
    requestMagicLoginLink,
    { loading: isMagicLinkRequestLoading, data: requestMagicLoginLinkData },
  ] = useRequestMagicLoginLinkMutation();
  const isAuthDialogOpen = useIsAuthDialogOpen();
  const setIsAuthDialogOpen = useSetIsAuthDialogOpen();
  const { error: loginError } = useAuth();

  const error =
    authMethod === AuthMethodEnum.Password
      ? loginError
      : requestMagicLoginLinkData?.requestMagicLoginLink?.error;

  const { register, handleSubmit, formState, reset } = useForm<LoginFormValues>({
    resolver: zodResolver(loginFormSchema),
  });

  const handleClose = () => {
    setIsAuthDialogOpen(false);
    setShowMagicLinkSent(false);
  };

  const onSubmit = async (data: LoginFormValues) => {
    const result = await login(data.email, data.password);
    if (result) {
      handleClose();
    }
  };

  const handleRequestMagicLoginLink = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);
    setRecipientEmail(data.get('email')?.toString() || '');
    const result = await requestMagicLoginLink({
      variables: {
        email: data.get('email')?.toString() || '',
      },
    });
    if (result.data?.requestMagicLoginLink?.success) {
      setShowMagicLinkSent(true);
    }
  };

  // Reset form when open dialog
  useEffect(() => {
    if (isAuthDialogOpen) {
      reset();
    }
  }, [isAuthDialogOpen, reset]);

  // Use `disableScrollLock` because it's applying inline body.styles.overflow style changes
  //   with something else at the same time, resulting in non-scrollable window
  return (
    <Dialog onClose={handleClose} open={isAuthDialogOpen} disableScrollLock>
      {showMagicLinkSent ? (
        <Alert
          severity="success"
          variant="filled"
          onClose={handleClose}
          icon={<MarkunreadMailboxIcon fontSize="inherit" />}
        >
          <AlertTitle>Magic link sent!</AlertTitle>
          Follow the link we sent you at {recipientEmail} to finish logging in!
        </Alert>
      ) : (
        <Box
          sx={{
            padding: 4,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in with SurveyStack
          </Typography>
          {authMethod === AuthMethodEnum.Password ? (
            <Box component="form" onSubmit={handleSubmit(onSubmit)} noValidate sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                fullWidth
                id="email"
                label="Email Address"
                autoComplete="email"
                autoFocus
                {...register('email')}
                error={!!formState.errors.email}
                helperText={formState.errors.email?.message}
              />
              <TextField
                margin="normal"
                fullWidth
                id="password"
                label="Password"
                type="password"
                autoComplete="current-password"
                {...register('password')}
                error={!!formState.errors.password}
                helperText={formState.errors.password?.message}
              />
              {/* <FormControlLabel
          control={<Checkbox value="remember" color="primary" />}
          label="Remember me"
        /> */}
              <LoadingButton
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                loading={isLoginInProgress}
              >
                Sign In
              </LoadingButton>
              {/* <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid> */}
            </Box>
          ) : (
            <Box component="form" onSubmit={handleRequestMagicLoginLink} noValidate sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
              />
              {/* <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          /> */}
              <LoadingButton
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                loading={isMagicLinkRequestLoading}
              >
                Send Link
              </LoadingButton>
            </Box>
          )}

          {error ? <Alert severity="error">{error}</Alert> : null}
          <Button
            variant="text"
            onClick={() =>
              setAuthMethod(
                authMethod === AuthMethodEnum.Password
                  ? AuthMethodEnum.MagicLink
                  : AuthMethodEnum.Password,
              )
            }
          >
            {authMethod === AuthMethodEnum.Password
              ? 'email me a sign in link instead'
              : 'sign in with password instead'}
          </Button>
          <Button
            fullWidth
            sx={{ mt: 3, mb: 2 }}
            endIcon={<ArrowForwardIcon />}
            onClick={handleClose}
          >
            Continue without login
          </Button>
        </Box>
      )}
    </Dialog>
  );
};

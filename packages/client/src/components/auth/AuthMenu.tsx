import React, { FunctionComponent, useEffect, useState } from 'react';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import HelpIcon from '@mui/icons-material/Help';
import LogoutIcon from '@mui/icons-material/Logout';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Stack from '@mui/material/Stack';
import { useAuth, useLogout } from 'states/auth';
import { useStartTour } from 'states/tour';
import { useSetIsAuthDialogOpen } from 'states/ui';

interface Props {
  children?: never;
}

export const AuthMenu: FunctionComponent<Props> = () => {
  const auth = useAuth();
  const setIsAuthDialogOpen = useSetIsAuthDialogOpen();
  const logout = useLogout();
  const startTour = useStartTour();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [menu, setMenu] = useState<'user' | 'help'>();

  const open = Boolean(anchorEl && menu);

  const handleClick = (menu: 'user' | 'help', el: HTMLElement) => {
    setAnchorEl(el);
    setMenu(menu);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setMenu(undefined);
  };

  const handleLearnMore = () => {
    window.open('https://www.our-sci.net/wp-content/uploads/2023/01/CoffeeShop.pdf', '_blank');
  };

  // Open SignIn dialog on start if user is not logged in
  useEffect(() => {
    if (!auth.isAuthenticated) {
      setIsAuthDialogOpen(true);
    }
  }, [auth.isAuthenticated, setIsAuthDialogOpen]);

  return (
    <>
      <Stack direction="row" alignItems="center">
        {auth.isAuthenticated ? (
          <IconButton
            id="basic-button"
            aria-controls={open ? 'basic-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
            onClick={(e) => handleClick('user', e.currentTarget)}
          >
            <AccountCircleIcon />
          </IconButton>
        ) : (
          <Button onClick={() => setIsAuthDialogOpen(true)}>Sign in</Button>
        )}
        <IconButton onClick={(e) => handleClick('help', e.currentTarget)}>
          <HelpIcon />
        </IconButton>
      </Stack>

      {menu === 'user' && auth.isAuthenticated && (
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          onClick={handleClose}
          anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
          transformOrigin={{ horizontal: 'right', vertical: 'top' }}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          <MenuItem>
            <ListItemText>{auth.user.email}</ListItemText>
          </MenuItem>
          <Divider />
          <MenuItem onClick={logout}>
            <ListItemIcon>
              <LogoutIcon />
            </ListItemIcon>
            <ListItemText>Logout</ListItemText>
          </MenuItem>
        </Menu>
      )}

      {menu === 'help' && (
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          onClick={handleClose}
          anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
          transformOrigin={{ horizontal: 'right', vertical: 'top' }}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          <MenuItem onClick={startTour}>
            <ListItemText>Start Tour</ListItemText>
          </MenuItem>
          <Divider />
          <MenuItem onClick={handleLearnMore}>
            <ListItemText>Learn More</ListItemText>
          </MenuItem>
        </Menu>
      )}
    </>
  );
};

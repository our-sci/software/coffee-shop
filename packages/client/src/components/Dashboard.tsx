/** @jsxImportSource @emotion/react */

import {
  ComponentProps,
  createRef,
  FunctionComponent,
  ReactNode,
  RefObject,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { useResizeDetector } from 'react-resize-detector';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { ApolloProvider } from '@apollo/client';
import { ClassNames } from '@emotion/core';
import { css, withTheme } from '@emotion/react';
import styled from '@emotion/styled';
import bgCorn from 'assets/images/Background-corngrains.jpg';
import logoImage from 'assets/images/Farmers-coffeeshop-logo-white_transparent.png';
import { client } from 'graphql/client';
import { RecoilRoot } from 'recoil';
import RecoilNexus from 'recoil-nexus';
import {
  useIsLoadingInitialUserData,
  useSetupUIToShowRelevantInfoToUser,
  useTryAcceptingMagicLinkLogin,
} from 'states/auth';
import { useSidePanelContent } from 'states/sidePanelContent';
import { StopEnum } from 'states/tour';
import { CropSelectorDialog } from './compareTab/CropSelectorDialog';
import { AuthMenu, LoginDialog } from './auth';
import { TourStop } from './common';
import { CompareTab } from './compareTab';
import { usePreloadDataQuery } from './Dashboard.generated';
import { FarmerProfile } from './FarmerProfile';
import { FilterEditor } from './filterEditor';
import { HyloBox } from './HyloBox';
import { Notifications } from './Notifications';
import { PlantingCardList } from './plantingCards';
import { Tabs } from './tabs';

const Background = withTheme(styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;
  ${(p) =>
    p.theme.useBackgroundImage
      ? `
    background-image: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 4) 0%,
      rgba(0, 0, 0, 0.34) 10%
    ),
    url(${bgCorn});
    background-size: cover;
    background-repeat: no-repeat;
    background-attachment: fixed;
`
      : ''}
`);

const Root = withTheme(styled.div`
  width: 100%;
  height: 100%;
  min-height: 100vh;
  padding-left: 52px;
  box-sizing: border-box;
  background-color: ${(p) => p.theme.colors.bg};
  display: grid;
  grid-template-columns: 1fr max(500px, 34%);
  grid-template-rows: 60px auto;
  grid-template-areas: 'values header' 'values events';
  & > * {
    overflow: hidden;
    max-width: 100%;
  }
`);

const Header = styled.div`
  grid-area: header;
  display: flex;
  justify-content: flex-end;
  padding: 0 12px;
  z-index: 1;
`;

const RightSide = styled.div`
  grid-area: events;
  position: relative;
`;

const RightFlipContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
`;

interface Props {
  iframeSrc: string;
  children?: never;
}

export const Dashboard: FunctionComponent<Props> = ({ iframeSrc }) => {
  useTryAcceptingMagicLinkLogin();

  const { ref, width = 0, height = 0 } = useResizeDetector();
  const { loading } = usePreloadDataQuery();
  const setupUi = useSetupUIToShowRelevantInfoToUser();
  const isLoadingInitialUserData = useIsLoadingInitialUserData();
  const sidePanelContent = useSidePanelContent();
  const [tabIndex, setTabIndex] = useState<number>(0);

  const pages = useMemo(
    () => [
      {
        label: 'Compare',
        renderPanel: () => <CompareTab />,
      },
    ],
    [],
  );

  const [SideContent, sideContentKey, nodeRef] = useMemo<
    [ReactNode, string, RefObject<HTMLDivElement>]
  >(
    () =>
      sidePanelContent.type === 'FilterEditor'
        ? [
            <FilterEditor selectedFilterId={sidePanelContent.filterId} />,
            `FilterEditor-${sidePanelContent.filterId}`,
            createRef(),
          ]
        : sidePanelContent.type === 'Profile'
        ? [
            <FarmerProfile producerId={sidePanelContent.producerId} />,
            `FarmerProfile-${sidePanelContent.producerId}`,
            createRef(),
          ]
        : [
            <PlantingCardList openEventCardIds={sidePanelContent.plantingIds} />,
            `PlantingCards`,
            createRef(),
          ],
    [sidePanelContent],
  );

  useEffect(() => {
    if (!loading) {
      setupUi();
    }
  }, [loading, setupUi]);

  return (
    <>
      <Background />
      <Root>
        <Backdrop
          sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={loading || isLoadingInitialUserData}
        >
          <CircularProgress />
        </Backdrop>
        <Header>
          <AuthMenu />
          <img
            css={css`
              height: 72%;
              width: auto;
              align-self: center;
            `}
            src={logoImage}
            width="180"
            alt=""
          />
        </Header>
        <Tabs
          css={css`
            grid-area: values;
            margin-top: 30px;
            height: fit-content;
            min-height: calc(100% - 60px);
            z-index: 1;
          `}
          pages={pages}
          index={tabIndex}
          onChange={setTabIndex}
        />
        <RightSide ref={ref}>
          <ClassNames>
            {({ css }) => (
              <TransitionGroup>
                <CSSTransition
                  in
                  key={sideContentKey}
                  nodeRef={nodeRef}
                  classNames={{
                    enter: css({
                      zIndex: 2,
                      opacity: 0,
                      transform: 'translateX(-2%);',
                    }),
                    enterActive: css({
                      opacity: 1,
                      transform: 'translateX(0%);',
                      transition: 'all 300ms ease-out',
                    }),
                    exit: css({ zIndex: 1 }),
                    exitActive: css({
                      opacity: 0,
                      transition: 'all 300ms  ease-in',
                    }),
                  }}
                  timeout={600}
                >
                  <RightFlipContainer ref={nodeRef}>{SideContent}</RightFlipContainer>
                </CSSTransition>
              </TransitionGroup>
            )}
          </ClassNames>

          <TourStop stop={StopEnum.DISCUSS} placement="left-start">
            <HyloBox src={iframeSrc} width={width} height={height} />
          </TourStop>
        </RightSide>

        <LoginDialog />

        <CropSelectorDialog />
      </Root>
    </>
  );
};

export const App = (props: ComponentProps<typeof Dashboard>) => (
  <ApolloProvider client={client}>
    <RecoilRoot>
      <RecoilNexus />
      <Dashboard {...props} />
      <Notifications />
    </RecoilRoot>
  </ApolloProvider>
);

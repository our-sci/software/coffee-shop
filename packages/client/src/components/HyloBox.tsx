/** @jsxImportSource @emotion/react */

import React, { forwardRef, useEffect, useState } from 'react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import styled from '@emotion/styled';

const HyloDragger = styled.div<{
  height: number;
  width: number;
  headerHeight: number;
  open: boolean;
  hovering: boolean;
}>`
  position: fixed;
  z-index: 1000;
  width: ${(p) => p.width}px;
  height: ${(p) => p.height}px;
  right: 0;
  top: ${(p) =>
    p.open
      ? `calc(100% - ${p.height}px)`
      : `calc(100% - ${p.headerHeight + (p.hovering ? 10 : 0)}px)`};
  background-color: #00c6b4;
  transition: all 0.3s cubic-bezier(0.14, -0.03, 0.49, 1.02);
`;

const HyloIFrame = styled.iframe`
  width: 100%;
  height: 100%;
`;

const HeaderClick = styled.div<{ open: boolean; headerHeight: number }>`
  height: ${(p) => (p.open ? `${p.headerHeight}px` : '100%')};
  width: 100%;
  position: absolute;
  top: 0px;
  cursor: pointer;
  pointer-events: ${(p) => (p.open ? 'none' : 'auto')};
`;

const CloseEar = styled.div<{ headerHeight: number }>`
  width: ${(p) => p.headerHeight}px;
  height: ${(p) => p.headerHeight}px;
  position: absolute;
  left: -${(p) => p.headerHeight}px;
  top: 0;
  pointer-events: none;
  overflow: hidden;
`;

const CloseEarContent = styled.div<{ open: boolean }>`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  left: ${(p) => (p.open ? '0' : '100%')};
  width: 100%;
  height: 100%;
  pointer-events: auto;
  border-radius: 5px 0 0 5px;
  background-color: white;
  transition: all 0.3s cubic-bezier(0.14, -0.03, 0.49, 1.02);
  box-shadow: inset rgba(0, 0, 0, 0.15) -7px 0 8px -7px;
  color: rgba(44, 64, 89, 0.6);

  svg {
    transition: all 0.1s ease; //cubic-bezier(0.14, -0.03, 0.49, 1.02);
  }
  :hover {
    color: rgba(44, 64, 89, 1);
    svg {
      transform: translateY(2px);
    }
  }
`;

export const HyloBox = forwardRef<
  HTMLDivElement,
  {
    src?: string;
    width: number;
    height: number;
  }
>(({ src = 'https://www.hylo.com/groups/loud-cacti/explore', width, height }, ref) => {
  const [open, setOpen] = useState(false);
  const [hovering, setHovering] = useState(false);

  // Delay loading the Holo frame so it doesn't take up bandwidth from the rest of the app
  const [isWaiting, setIsWaiting] = useState(true);
  useEffect(() => {
    setTimeout(() => setIsWaiting(false), 5000);
  }, []);

  if (width === 0 || height === 0) {
    return null;
  }

  return (
    <HyloDragger
      ref={ref}
      width={width}
      height={height}
      headerHeight={56}
      open={open}
      hovering={hovering}
    >
      {!isWaiting && <HyloIFrame src={src} title="Hylo Group" />}
      <HeaderClick
        open={open}
        headerHeight={56}
        onClick={() => setOpen(!open)}
        onMouseEnter={() => setHovering(true)}
        onMouseLeave={() => setHovering(false)}
      />

      <CloseEar headerHeight={56} onClick={() => setOpen(false)}>
        <CloseEarContent open={open}>
          <ExpandMoreIcon fontSize="large" color="inherit" />
        </CloseEarContent>
      </CloseEar>
    </HyloDragger>
  );
});

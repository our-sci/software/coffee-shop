import { FunctionComponent, MouseEventHandler, useMemo } from 'react';
import CancelIcon from '@mui/icons-material/Cancel';
import Button from '@mui/material/Button';
import createTheme from '@mui/material/styles/createTheme';
import ThemeProvider from '@mui/material/styles/ThemeProvider';
import { useRemoveFilter } from 'states/filters';
import { useHighlightFilter, useUnhighlightFilter } from 'states/highlightedFilterId';
import { useShowFilterEditor, useShowPlantingCards } from 'states/sidePanelContent';

export const defaultTheme = {};

interface Props {
  color: string;
  label: string;
  filterId: string;
  children?: never;
}

export const FilterLabel: FunctionComponent<Props> = ({ label, color = 'green', filterId }) => {
  const showFilterEditor = useShowFilterEditor();
  const highlightFilter = useHighlightFilter();
  const unhighlightFilter = useUnhighlightFilter();
  const removeFilter = useRemoveFilter();
  const showPlantingCards = useShowPlantingCards();

  const handleSelect = () => {
    showFilterEditor(filterId);
  };

  const handleRemove: MouseEventHandler<SVGElement> = (e) => {
    e.preventDefault();
    e.stopPropagation();
    removeFilter(filterId);
    showPlantingCards();
  };

  const theme = useMemo(() => createTheme({ palette: { primary: { main: color } } }), [color]);

  return (
    <ThemeProvider theme={theme}>
      <Button
        color="primary"
        variant="contained"
        size="small"
        sx={{ borderRadius: 3, overflow: 'hidden' }}
        onMouseEnter={() => highlightFilter(filterId)}
        onMouseLeave={() => unhighlightFilter(filterId)}
        onClick={handleSelect}
        endIcon={
          <CancelIcon sx={{ opacity: 0.8, '&:hover': { opacity: 1 } }} onClick={handleRemove} />
        }
      >
        {label}
      </Button>
    </ThemeProvider>
  );
};

/** @jsxImportSource @emotion/react */

import React, { ChangeEvent, ReactNode, useMemo } from 'react';
import LinearProgress from '@mui/material/LinearProgress';
import TextField from '@mui/material/TextField';
import { withTheme } from '@emotion/react';
import styled from '@emotion/styled';
import { Spacer } from 'components/common';
import { isOptionFilterParam, isRangeFilterParam } from 'models/Filter';
import { useFilters, useUpdateFilterName } from 'states/filters';
import { useSelectedCropType } from 'states/selectedCropType';
import { getFilterables, isNumericFilterable, isOptionFilterable } from 'utils/filterables';
import { useFilterEditorQuery } from './FilterEditor.generated';
import { FilterMenu } from './FilterMenu';
import { FilterParamSelector } from './FilterParamSelector';
import { InputActionsWrap } from './InputActionsWrap';
import { RangeSlider } from './RangeSlider';
import { TagSelect } from './TagSelect';

const Root = withTheme(styled.div`
  background-color: ${(p) => p.theme.colors.bgSidePanel};
  display: flex;
  flex-direction: column;
`);

const Body = withTheme(styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 8px;
  padding: 0 12px 20px;
  & > :not(.range) + .range,
  & > .range + :not(.range) {
    margin-top: 8px;
  }
  & > .range + .range {
    margin-top: 0px;
  }
  & > :not(.range) + :not(.range) {
    margin-top: 16px;
  }
`);

const Header = withTheme(styled.div<{ color: string }>`
  display: flex;
  align-items: center;
  font-family: ${(p) => p.theme.font};
  font-size: 19px;
  padding: 8px 14px;
`);

const Title = withTheme(styled.div`
  font-weight: 600;
  color: white;
`);

/**
 * Primary UI component for user interaction
 */
interface Props {
  selectedFilterId: string;
}

export const FilterEditor = ({ selectedFilterId }: Props) => {
  const filters = useFilters();
  const selectedCropType = useSelectedCropType();
  const filter = useMemo(
    () => filters.find((filter) => filter.id === selectedFilterId),
    [selectedFilterId, filters],
  );
  const updateFilterName = useUpdateFilterName();
  const { plantings = [] } =
    useFilterEditorQuery({
      variables: { cropType: selectedCropType },
    }).data || {};

  const filterables = useMemo(() => getFilterables(plantings), [plantings]);

  const updateName = (e: ChangeEvent<HTMLInputElement>) => {
    if (filter) {
      updateFilterName(filter.id, e.target.value);
    }
  };

  const filterNodes = useMemo<ReactNode[]>(
    () =>
      filter?.params
        .map((param) => {
          const filterable = filterables.find(
            (f) => f.key === param.key && f.dataSource === param.dataSource,
          );
          const isTag =
            isOptionFilterParam(param) && (!filterable || isOptionFilterable(filterable));
          const isRange =
            isRangeFilterParam(param) && (!filterable || isNumericFilterable(filterable));
          const node = isTag ? (
            <TagSelect filterId={selectedFilterId} filterable={filterable} param={param} />
          ) : isRange ? (
            <RangeSlider filterId={selectedFilterId} filterable={filterable} param={param} />
          ) : null;

          return node ? (
            <InputActionsWrap
              key={param.key}
              className={isRange ? 'range' : undefined}
              filterId={selectedFilterId}
              paramKey={param.key}
            >
              {node}
            </InputActionsWrap>
          ) : null;
        })
        .filter(Boolean) ?? [],
    [filter?.params, filterables, selectedFilterId],
  );

  if (!filter) {
    return (
      <Root>
        <LinearProgress />
      </Root>
    );
  }

  return (
    <Root>
      <Header color={filter.color}>
        <Title>{`Filter options${filter.name ? ` - ${filter.name}` : ''}`}</Title>
        <Spacer />
        <FilterMenu />
      </Header>
      <Body>
        <TextField
          label="Filter Name"
          variant="outlined"
          value={filter.name}
          onChange={updateName}
        />
        <FilterParamSelector
          filterId={filter.id}
          filterables={filterables}
          params={filter.params}
        />
        {filterNodes}
      </Body>
    </Root>
  );
};

import { FunctionComponent, useMemo } from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import Badge, { BadgeProps } from '@mui/material/Badge';
import styled from '@mui/material/styles/styled';
import TextField from '@mui/material/TextField';
import uniq from 'lodash/uniq';
import { FilterableOption, FilterParam, FilterValueOption } from 'models/Filter';
import { useEditFilterParam } from 'states/filters';
import { formatValue, prettyKey } from 'utils/format';

const StyledBadge = styled(Badge)<BadgeProps>(() => ({
  '& .MuiBadge-badge': {
    right: -20,
    top: 13,
    padding: '0 4px',
  },
}));

interface Props {
  filterable?: FilterableOption;
  param: FilterParam & { value: FilterValueOption };
  filterId: string;
  children?: never;
}

export const TagSelect: FunctionComponent<Props> = ({ filterable, param, filterId }) => {
  const editFilterParam = useEditFilterParam();

  const options = useMemo(
    () => uniq([...(filterable?.options.map((o) => o.value) || []), ...param.value.options]).sort(),
    [filterable?.options, param.value.options],
  );

  const occurrences = useMemo(
    () => Object.fromEntries((filterable?.options || []).map((o) => [o.value, o.occurrences])),
    [filterable?.options],
  );

  const handleChange = (_: any, options: string[]) => {
    editFilterParam(filterId, param.key, { ...param.value, options });
  };

  return (
    <Autocomplete
      disabled={!filterable}
      multiple
      onChange={handleChange}
      options={options}
      value={param.value.options}
      getOptionLabel={(option) => formatValue(option)}
      disableCloseOnSelect
      disableClearable
      renderOption={(props, option) => (
        <li {...props}>
          <StyledBadge badgeContent={occurrences[option] || 0} color="secondary" showZero>
            {formatValue(option)}
          </StyledBadge>
        </li>
      )}
      renderInput={(params) => (
        <TextField {...params} variant="outlined" label={prettyKey(param.key)} />
      )}
    />
  );
};

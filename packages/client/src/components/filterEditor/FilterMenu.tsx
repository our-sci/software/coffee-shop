import React, { FunctionComponent } from 'react';
import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import Tooltip from '@mui/material/Tooltip';
import { useShowPlantingCards } from 'states/sidePanelContent';

interface Props {
  children?: never;
}

export const FilterMenu: FunctionComponent<Props> = () => {
  const showPlantingCards = useShowPlantingCards();

  return (
    <Stack direction="row" justifyContent="center" alignItems="center" spacing={0}>
      <Tooltip title="Close Filter Editor">
        <IconButton onClick={showPlantingCards}>
          <CloseIcon />
        </IconButton>
      </Tooltip>
    </Stack>
  );
};

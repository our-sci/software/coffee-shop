import React, { FunctionComponent, ReactNode } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import Tooltip from '@mui/material/Tooltip';
import { useRemoveFilterParam } from 'states/filters';

interface Props {
  className?: string;
  filterId: string;
  paramKey: string;
  children?: ReactNode;
}

export const InputActionsWrap: FunctionComponent<Props> = ({
  className,
  filterId,
  paramKey,
  children,
}) => {
  const removeFilterParam = useRemoveFilterParam();

  const remove = () => removeFilterParam(filterId, paramKey);

  return (
    <Stack
      direction="row"
      justifyContent="center"
      alignItems="center"
      spacing={1}
      className={className}
    >
      <Box flexGrow={1}>{children}</Box>
      <Tooltip title="Remove Filter Parameter">
        <IconButton onClick={remove}>
          <DeleteIcon />
        </IconButton>
      </Tooltip>
    </Stack>
  );
};

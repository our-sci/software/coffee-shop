import { FunctionComponent, useMemo } from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import styled from '@mui/material/styles/styled';
import Typography from '@mui/material/Typography';
import { format } from 'd3-format';
import sortBy from 'lodash/sortBy';
import { FilterableNumeric, FilterParam, FilterValueRange } from 'models/Filter';
import { useEditFilterParam } from 'states/filters';
import { prettyKey } from 'utils/format';

const TickedSlider = styled(Slider)({
  '& .MuiSlider-mark': {
    height: 4,
    width: 2,
    opacity: 0.4,
  },
  '& .MuiSlider-markLabel': {
    fontSize: '0.75rem',
  },
});

const Label = styled(Typography)({
  color: 'rgba(255, 255, 255, 0.7)',
});

interface Props {
  filterable?: FilterableNumeric;
  param: FilterParam & { value: FilterValueRange };
  filterId: string;
  children?: never;
}

export const RangeSlider: FunctionComponent<Props> = ({ filterable, param, filterId }) => {
  const editFilterParam = useEditFilterParam();
  const allValues = useMemo(() => sortBy(filterable?.values ?? []), [filterable?.values]);
  const intMode = useMemo<boolean>(
    () => allValues.length > 0 && allValues.every(Number.isInteger),
    [allValues],
  );
  const min = useMemo<number>(
    () => Math.min(param.value.min, ...allValues),
    [allValues, param.value.min],
  );
  const max = useMemo<number>(
    () => Math.max(param.value.max, ...allValues),
    [allValues, param.value.max],
  );
  const formatter = useMemo(() => (intMode ? format('') : format('.1f')), [intMode]);
  const step = intMode ? 1 : 0.01;
  const marks = useMemo(
    () =>
      allValues.map((value, i) =>
        i === 0 || i === allValues.length - 1 ? { value, label: formatter(value) } : { value },
      ),
    [allValues, formatter],
  );

  const handleChange = (_: any, value: number | number[]) => {
    if (Array.isArray(value)) {
      editFilterParam(filterId, param.key, {
        min: value[0],
        max: value[1],
      });
    }
  };

  return (
    <Box mx={1}>
      <Label variant="caption">{prettyKey(param.key)}</Label>
      <TickedSlider
        disabled={!filterable}
        getAriaLabel={() => prettyKey(param.key)}
        getAriaValueText={formatter}
        value={[param.value.min, param.value.max]}
        onChange={handleChange}
        valueLabelDisplay="auto"
        step={step}
        min={min}
        max={max}
        marks={marks}
        sx={{
          marginTop: -0.5,
        }}
      />
    </Box>
  );
};

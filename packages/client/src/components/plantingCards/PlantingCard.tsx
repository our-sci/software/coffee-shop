/** @jsxImportSource @emotion/react */

import { FunctionComponent, useMemo } from 'react';
import CloseIcon from '@mui/icons-material/Close';
import ContactPageIcon from '@mui/icons-material/ContactPage';
import InvertColorsIcon from '@mui/icons-material/InvertColors';
import PublicIcon from '@mui/icons-material/Public';
import PushPinIcon from '@mui/icons-material/PushPin';
import PushPinOutlinedIcon from '@mui/icons-material/PushPinOutlined';
import ThermostatIcon from '@mui/icons-material/Thermostat';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import Tooltip from '@mui/material/Tooltip';
import { withTheme } from '@emotion/react';
import styled from '@emotion/styled';
import { Spacer } from 'components/common';
import isNil from 'lodash/isNil';
import {
  useHighlightedPlantingId,
  useHighlightPlanting,
  useUnhighlightPlanting,
} from 'states/highlightedPlantingId';
import { usePinnedPlantingIds, usePinPlanting, useUnpinPlanting } from 'states/pinnedPlantingIds';
import { useRemovePlantingCard, useShowProfile } from 'states/sidePanelContent';
import tinycolor from 'tinycolor2';
import { climateRegionToLongName } from 'utils/abbreviations';
import { IconEventsBar } from './IconEventsBar';
import { PlantingCardListQuery } from './PlantingCardList.generated';

export const defaultTheme = {
  sidePad: 10,
  colorBorderWidth: 15,
  colorBorderHighlightWidth: 20,
  hoverExtraWidth: 0,
  parametersFontSize: 12,
  colorBorderStrideWidth: 12,
  colorBorderStrideAngle: -34,
};

const Root = withTheme(styled.div<{
  isHighlighted: boolean;
}>`
  display: flex;
  flex-wrap: nowrap;
  background-color: ${(p) =>
    p.isHighlighted
      ? tinycolor(p.theme.colors.bgSidePanel).lighten(10).toString()
      : p.theme.colors.bgSidePanel};
  width: ${(p) =>
    p.isHighlighted ? `calc(100% + ${p.theme.eventsCard.hoverExtraWidth}px)` : '100%'};
  margin-bottom: 20px;
  min-height: 70px;
`);

const ColorBorder = withTheme(styled.div<{
  colors: string[];
  isHighlighted: boolean;
}>`
  flex: 0;
  flex-basis: ${(p) =>
    p.isHighlighted
      ? p.theme.eventsCard.colorBorderHighlightWidth
      : p.theme.eventsCard.colorBorderWidth}px;
  background: ${(p) =>
    p.isHighlighted
      ? 'white'
      : `repeating-linear-gradient(
      ${p.theme.eventsCard.colorBorderStrideAngle}deg,
      ${p.colors
        .map((color, i) => [
          `${color} ${i * p.theme.eventsCard.colorBorderStrideWidth}px`,
          `${color} ${(i + 1) * p.theme.eventsCard.colorBorderStrideWidth - 2}px`,
          `${p.colors[(i + 1) % p.colors.length]} ${
            (i + 1) * p.theme.eventsCard.colorBorderStrideWidth
          }px`,
        ])
        .flat()
        .join(',')}
    )`};

  transition: all 0.1s ease-out;
`);

const Head = withTheme(styled.div`
  display: flex;
  border-bottom: 2px solid rgba(255, 255, 255, 0.5);
  align-items: flex-start;
  gap: 5px;
  font-size: 18px;
  padding: 7px ${(p) => p.theme.eventsCard.sidePad}px 2px;
`);

const Title = withTheme(styled.div`
  margin-right: auto;
  font-family: ${(p) => p.theme.font};
  color: white;
`);

const Name = withTheme(styled.div`
  display: flex;
  align-items: center;
  gap: 1px;
  font-family: ${(p) => p.theme.font};
  font-weight: 600;
  color: white;
  cursor: pointer;
  :hover {
    text-decoration: underline;
  }
`);

const Params = withTheme(styled.div`
  display: flex;
  padding: 1px 10px;
  font-size: ${(p) => p.theme.eventsCard.parametersFontSize}px;
`);
const ParamValue = withTheme(styled.div`
  font-family: ${(p) => p.theme.font};
  font-weight: 600;
  color: ${(p) => p.theme.colors.secondary};
`);

const IconButton = styled.div<{ disabled?: boolean }>`
  width: 24px;
  height: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
  color: white;
  cursor: pointer;
  opacity: 0.7;
  :hover {
    opacity: 1;
  }
`;

const MiniInfoContent = styled.div`
  display: flex;
  align-items: center;
  margin-right: 8px;
`;

const MiniInfo: FunctionComponent<{ tooltip: string }> = ({ tooltip, children }) => (
  <Tooltip title={tooltip} arrow>
    <MiniInfoContent>{children}</MiniInfoContent>
  </Tooltip>
);

interface Props {
  planting: PlantingCardListQuery['plantings'][number];
  hideName?: boolean;
  hideColorBorder?: boolean;
  minEventDate?: Date;
  maxEventDate?: Date;
  colors?: string[];
  children?: never;
}

export const PlantingCard: FunctionComponent<Props> = ({
  planting,
  hideName = false,
  hideColorBorder = false,
  minEventDate,
  maxEventDate,
  colors,
}) => {
  const highlightedPlantingId = useHighlightedPlantingId();
  const unhighlightPlanting = useUnhighlightPlanting();
  const highlightPlanting = useHighlightPlanting();
  const pinnedPlantingIds = usePinnedPlantingIds();
  const pinPlanting = usePinPlanting();
  const unpinPlanting = useUnpinPlanting();
  const removePlantingCard = useRemovePlantingCard();
  const showProfile = useShowProfile();

  const isHighlighted = planting.id === highlightedPlantingId;
  const isPinned = pinnedPlantingIds.includes(planting.id);

  const { averageAnnualTemperature, averageAnnualRainfall, climateZone, hardinessZone } =
    planting?.farmOnboarding || {};

  const texture = useMemo(() => {
    const { sandPercentage, clayPercentage, soilTexture } = planting?.params || {};
    const sand = isNil(sandPercentage) ? null : `Sand ${sandPercentage}%`;
    const clay = isNil(clayPercentage) ? null : `Clay ${clayPercentage}%`;
    const texture = isNil(soilTexture) ? null : `Texture ${soilTexture}`;
    return [texture, sand, clay].filter(Boolean).join(' | ');
  }, [planting?.params]);

  const onClose = () => {
    removePlantingCard(planting.id);
    unpinPlanting(planting.id);
    unhighlightPlanting();
  };

  const onHoverData = () => {
    highlightPlanting(planting.id);
  };

  const onLeaveData = () => {
    unhighlightPlanting();
  };

  const onPin = () => {
    if (isPinned) {
      unpinPlanting(planting.id);
    } else {
      pinPlanting(planting.id);
    }
  };

  return (
    <Root isHighlighted={isHighlighted} onMouseEnter={onHoverData} onMouseLeave={onLeaveData}>
      <div style={{ flex: 1 }}>
        <Head>
          <Title>{planting.title}</Title>
          {!hideName && (
            <Tooltip title="Producer profile">
              <Name onClick={() => showProfile(planting.producer.id)}>
                <ContactPageIcon fontSize="inherit" />
                {planting.producer.code}
              </Name>
            </Tooltip>
          )}
          <IconButton style={{ marginLeft: 10, opacity: isPinned ? 1 : undefined }} onClick={onPin}>
            {isPinned ? (
              <PushPinIcon sx={{ width: 22, height: 22 }} />
            ) : (
              <PushPinOutlinedIcon sx={{ width: 22, height: 22 }} />
            )}
          </IconButton>
          {onClose && (
            <IconButton onClick={onClose}>
              <CloseIcon />
            </IconButton>
          )}
        </Head>

        <Params>
          <MiniInfo tooltip="Yearly average temperature">
            <ThermostatIcon fontSize="inherit" />
            <ParamValue>
              {isNil(averageAnnualTemperature) ? 'n/a' : `${averageAnnualTemperature}℃`}
            </ParamValue>
          </MiniInfo>
          <MiniInfo tooltip="Yearly average rainfall">
            <InvertColorsIcon fontSize="inherit" />
            <ParamValue>
              {isNil(averageAnnualRainfall) ? 'n/a' : `${averageAnnualRainfall}″`}
            </ParamValue>
          </MiniInfo>
          <MiniInfo tooltip="Hardiness zone">
            <WbSunnyIcon fontSize="inherit" />
            <ParamValue>{isNil(hardinessZone) ? 'n/a' : hardinessZone}</ParamValue>
          </MiniInfo>
          <MiniInfo
            tooltip={`Climate region: ${
              climateZone ? climateRegionToLongName(climateZone) : 'n/a'
            }`}
          >
            <PublicIcon fontSize="inherit" />
            <ParamValue>{isNil(climateZone) ? 'n/a' : climateZone}</ParamValue>
          </MiniInfo>
          <Spacer />
          <ParamValue>{texture}</ParamValue>
        </Params>
        <IconEventsBar
          minEventDate={minEventDate}
          maxEventDate={maxEventDate}
          events={planting.events}
        />
      </div>

      {!hideColorBorder && (
        <ColorBorder
          colors={colors?.length ? colors : ['grey']}
          isHighlighted={isHighlighted || isPinned}
        />
      )}
    </Root>
  );
};

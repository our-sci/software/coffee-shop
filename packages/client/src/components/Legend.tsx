import { FunctionComponent } from 'react';
import { withTheme } from '@emotion/react';
import styled from '@emotion/styled';

const Root = withTheme(styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  padding: 8px 28px;
  background-color: ${(props) => props.theme.colors.darkTransparent};
`);

const ColorDot = withTheme(styled.div<{ color: string }>`
  background-color: ${(props) => props.theme.color(props.color)};
  height: 16px;
  width: 16px;
  border-radius: 50%;
`);

const Tag = withTheme(styled.div`
  flex: 0;
  display: flex;
  gap: 10px;
  min-width: 33%;
  font-size: 12px;
  font-family: ${(props) => props.theme.fonts.baseBold};
  text-transform: uppercase;
  line-height: 20px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 2px 4px;
  color: white;
`);

interface Props {
  entries: {
    color: string;
    name: string;
  }[];
  className?: string;
  children?: never;
}

/**
 * Legend component
 */
export const Legend: FunctionComponent<Props> = ({ entries, className }) => {
  return (
    <Root className={className}>
      {entries.map((entry, index) => (
        <Tag key={index}>
          <ColorDot color={entry.color} />
          {entry.name}
        </Tag>
      ))}
    </Root>
  );
};

import { FunctionComponent } from 'react';
import CloseIcon from '@mui/icons-material/Close';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Snackbar from '@mui/material/Snackbar';
import { NotificationTypeEnum } from 'models/Notification';
import { useNotifications, useRemoveAllNotifications, useRemoveNotification } from 'states/ui';

interface Props {
  children?: never;
}

export const Notifications: FunctionComponent<Props> = () => {
  const notifications = useNotifications();
  const removeAllNotifications = useRemoveAllNotifications();
  const removeNotification = useRemoveNotification();

  const [first] = notifications;

  if (!first) {
    return null;
  }

  return (
    <Snackbar open={true} onClose={() => removeNotification(first.id)}>
      <Alert
        severity={first.type === NotificationTypeEnum.ERROR ? 'error' : 'info'}
        sx={{ width: '100%' }}
        action={[
          notifications.length > 1 ? (
            <Button key="remove-all" color="inherit" size="small" onClick={removeAllNotifications}>
              Close all ({notifications.length})
            </Button>
          ) : null,
          <IconButton
            key="remove"
            color="inherit"
            size="small"
            onClick={() => removeNotification(first.id)}
          >
            <CloseIcon />
          </IconButton>,
        ]}
      >
        {first.message}
      </Alert>
    </Snackbar>
  );
};

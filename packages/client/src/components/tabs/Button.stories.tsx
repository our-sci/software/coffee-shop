import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { withEditableTheme } from 'theme/withEditableTheme';
import { Button } from './Button';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Example/Button',
  component: Button,
} as ComponentMeta<typeof Button>;

const ButtonWithTheme = withEditableTheme(Button);

const Template: ComponentStory<typeof ButtonWithTheme> = (args) => <ButtonWithTheme {...args} />;

export const Default = Template.bind({});
Default.args = {
  label: '+ Add',
};

export const Wide = Template.bind({});
Wide.args = {
  label: 'Button Text',
  color: 'teal',
  isWide: true,
};

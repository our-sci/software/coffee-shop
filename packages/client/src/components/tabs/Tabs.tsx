/** @jsxImportSource @emotion/react */

import { ComponentProps, FunctionComponent, ReactNode } from 'react';
import { css, useTheme, withTheme } from '@emotion/react';
import styled from '@emotion/styled';
import tinycolor from 'tinycolor2';
import { Button } from './Button';

const Root = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const Head = styled.div`
  flex: 0;
  display: flex;
  justify-content: flex-start;
`;

const Body = withTheme(styled.div`
  flex: 1;
  background-color: ${(p) => p.theme.colors.bgTab};
  border: 1px solid ${(p) => p.theme.colors.divider};
`);

const Tab = (props: { active: boolean } & ComponentProps<typeof Button>) => {
  const { colors } = useTheme();
  const { active, ...buttonProps } = props;
  const color = active ? colors.bgTab : colors.bgSidePanel;

  return (
    <Button
      {...{ ...buttonProps, color }}
      css={css`
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
        border: ${active ? 1 : 0}px solid ${colors.divider};
        border-bottom: none;
        ${active ? `box-shadow: 0px 1px 0px 0px ${colors.bgTab}; z-index: 1;` : ''}
        :hover {
          background: linear-gradient(
            to bottom,
            ${tinycolor(color).lighten(7).toString()},
            ${color}
          );
        }
        :active {
          background: linear-gradient(
            to bottom,
            ${tinycolor(color).lighten(10).toString()},
            ${color}
          );
        }
      `}
    />
  );
};

interface Page {
  label: string;
  renderPanel: () => ReactNode;
}

/**
 * Primary UI component for user interaction
 */
interface Props {
  index: number;
  onChange: (index: number) => void;
  pages: Page[];
  className?: string;
  children?: never;
}

export const Tabs: FunctionComponent<Props> = ({ pages, index, onChange, className }) => {
  return (
    <Root className={className}>
      <Head>
        {pages.map((page, tabIndex) => (
          <Tab
            key={tabIndex}
            onClick={() => onChange(tabIndex)}
            label={page.label}
            active={tabIndex === index}
            isWide
          />
        ))}
      </Head>
      <Body>{pages[index]?.renderPanel()}</Body>
    </Root>
  );
};

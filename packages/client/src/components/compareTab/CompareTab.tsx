import { FunctionComponent } from 'react';
import styled from '@emotion/styled';
import { Spacer, TourStop } from 'components/common';
import { FilterLabel } from 'components/filterEditor';
import { useFilters } from 'states/filters';
import { useSelectedCropType, useSetSelectedCropType } from 'states/selectedCropType';
import { StopEnum } from 'states/tour';
import { NestedRows } from './valueRows/NestedRows';
import { AddFilterButton } from './AddFilterButton';
import { CropSelector } from './CropSelector';

const PaneHead = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;
  gap: 15px;
  margin: 12px 0px 8px;
`;

const RowContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 12px 20px 20px 20px;
`;

const LabelContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 2px;
  justify-content: center;
  padding: 4px 0px;
`;

interface Props {
  children?: never;
}

export const CompareTab: FunctionComponent<Props> = () => {
  const filters = useFilters();
  const value = useSelectedCropType();
  const setSelectedCropType = useSetSelectedCropType();

  return (
    <RowContainer>
      <PaneHead>
        <TourStop stop={StopEnum.SELECT_CROP} placement="right">
          <CropSelector
            sx={{ minWidth: 120 }}
            size="small"
            value={value}
            onChange={setSelectedCropType}
          />
        </TourStop>
        <Spacer />
        <LabelContainer>
          {[...filters].reverse().map((filter) => (
            <FilterLabel
              key={filter.id}
              filterId={filter.id}
              label={filter.name}
              color={filter.color}
            />
          ))}
        </LabelContainer>
        <TourStop stop={StopEnum.FILTER} placement="bottom-start">
          <AddFilterButton />
        </TourStop>
      </PaneHead>

      <NestedRows filters={filters} />
    </RowContainer>
  );
};

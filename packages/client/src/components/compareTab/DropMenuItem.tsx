import { ChangeEvent, FunctionComponent, MouseEvent, useMemo, useState } from 'react';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Checkbox from '@mui/material/Checkbox';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import ListItemText, { ListItemTextProps } from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import styled from '@mui/material/styles/styled';
import { Filter } from 'models/Filter';
import { Menu } from 'models/Menu';
import {
  createOptionFilterParam,
  useAddFilter,
  useFindFilter,
  useRemoveFilter,
} from 'states/filters';

const MenuLabel = styled(ListItemText)<ListItemTextProps>({
  '& .MuiListItemText-primary': {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
});

interface Props {
  type: 'farmDomain' | 'organization';
  item: Menu;
  level?: number;
}

const getFilterOptions = (type: Props['type'], name: string): Partial<Filter> => ({
  name: type === 'farmDomain' ? name.split('.')[0] : name,
  params: [createOptionFilterParam(type, [name])],
});

const flatChildren = (parent: Menu): Menu[] => {
  const children: Menu[] = [];
  parent.children.forEach((child) => {
    children.push({ ...child, children: [] });
    children.push(...flatChildren(child));
  });
  return children;
};

export const DropMenuItem: FunctionComponent<Props> = ({ item, level = 1, type }) => {
  const [open, setOpen] = useState<boolean>(false);
  const addFilter = useAddFilter();
  const removeFilter = useRemoveFilter();
  const { findFilter } = useFindFilter();

  const children = useMemo<Menu[]>(() => flatChildren(item), [item]);
  const hasChildren = children.length > 0;
  const options = useMemo<Partial<Filter>>(
    () => getFilterOptions(type, item.name),
    [item.name, type],
  );

  const filter = useMemo<Filter | undefined>(() => findFilter(options), [findFilter, options]);

  const isIndeterminate = useMemo<boolean>(() => {
    const childrenFilterCount = children.filter(
      (child) => !!findFilter(getFilterOptions(type, child.name)),
    ).length;

    return (
      hasChildren &&
      !!filter &&
      childrenFilterCount !== children.filter((child) => !child.disabled).length
    );
  }, [children, filter, findFilter, hasChildren, type]);

  const handleOpen = (e: MouseEvent) => {
    e.stopPropagation();
    setOpen((prev) => !prev);
  };

  const handleCheckTree = (e: ChangeEvent<HTMLInputElement>) => {
    const on = e.target.checked;
    children
      .filter((child) => !child.disabled)
      .forEach((child) => {
        const options = getFilterOptions(type, child.name);
        const match = findFilter(options);
        if (on && !match) {
          addFilter(options);
        } else if (!on && match) {
          removeFilter(match.id);
        }
      });
  };
  const handleCheckSelf = () => {
    if (filter) {
      removeFilter(filter.id);
    } else {
      addFilter(options);
    }
  };

  const handleClick = (e: MouseEvent) => {
    if (item.disabled) {
      return;
    } else if (item.id) {
      handleCheckSelf();
    } else {
      handleOpen(e);
    }
  };

  return (
    <>
      <MenuItem onClick={handleClick} sx={{ pl: level * 2, pr: 2 }}>
        {item.id && (
          <Checkbox
            checked={!!filter}
            indeterminate={isIndeterminate}
            onChange={handleCheckTree}
            size="small"
            disabled={item.disabled}
          />
        )}
        <MenuLabel>{item.name}</MenuLabel>
        {hasChildren && (
          <IconButton
            sx={{ marginLeft: 1 }}
            size="small"
            onClick={handleOpen}
            onMouseDown={(e) => e.stopPropagation()}
          >
            {open ? <ExpandLess /> : <ExpandMore />}
          </IconButton>
        )}
      </MenuItem>

      {hasChildren && (
        <Collapse in={open} timeout="auto" unmountOnExit>
          {item.children.map((child) => (
            <DropMenuItem key={child.id} item={child} level={level + 1} type={type} />
          ))}
        </Collapse>
      )}
    </>
  );
};

import React, { FunctionComponent, useEffect, useState } from 'react';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import GrassOutlinedIcon from '@mui/icons-material/GrassOutlined';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import { useSetSelectedCropType } from 'states/selectedCropType';
import {
  useIsAuthDialogOpen,
  useIsCropSelectorDialogOpen,
  useSetIsCropSelectorDialogOpen,
} from 'states/ui';
import { CropSelector } from './CropSelector';

export const CropSelectorDialog: FunctionComponent = () => {
  const isAuthDialogOpen = useIsAuthDialogOpen();
  const isCropSelectorDialogOpen = useIsCropSelectorDialogOpen();
  const setIsCropSelectorDialogOpen = useSetIsCropSelectorDialogOpen();
  const setSelectedCropType = useSetSelectedCropType();

  const [value, setValue] = useState<string>('');

  const handleExplore = () => {
    setSelectedCropType(value);
    handleClose();
  };

  const handleClose = () => {
    setIsCropSelectorDialogOpen(false);
  };

  useEffect(() => {
    if (isCropSelectorDialogOpen) {
      setValue('');
    }
  }, [isCropSelectorDialogOpen]);

  return (
    <Dialog
      open={isCropSelectorDialogOpen && !isAuthDialogOpen}
      onClose={handleClose}
      disableScrollLock
    >
      <Box
        sx={{
          padding: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <GrassOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Select a crop that you want to explore
        </Typography>
        <CropSelector fullWidth sx={{ mt: 2 }} value={value} onChange={setValue} />
        <Button
          fullWidth
          variant="contained"
          disabled={!value}
          onClick={handleExplore}
          sx={{ mt: 6 }}
        >
          Explore
        </Button>
        <Button fullWidth endIcon={<ArrowForwardIcon />} onClick={handleClose} sx={{ mt: 2 }}>
          Continue without selection
        </Button>
      </Box>
    </Dialog>
  );
};

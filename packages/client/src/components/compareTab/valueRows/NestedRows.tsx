import { FunctionComponent, useCallback, useMemo, useState } from 'react';
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import { TourStop } from 'components/common';
import findLastIndex from 'lodash/findLastIndex';
import last from 'lodash/last';
import { Filter } from 'models/Filter';
import { RowData } from 'models/RowData';
import { useHighlightedFilterId } from 'states/highlightedFilterId';
import { useHighlightedPlantingId } from 'states/highlightedPlantingId';
import { usePinnedPlantingIds } from 'states/pinnedPlantingIds';
import { useSelectedCropType } from 'states/selectedCropType';
import { StopEnum } from 'states/tour';
import { getPlantingIdsOfFilter } from 'utils/plantingsOfFilter';
import { covertNormalizedRows } from 'utils/rows';
import { isNonNull } from 'utils/validate';
import { NestedRowsQuery, useNestedRowsQuery } from './NestedRows.generated';
import { ValueDistribution } from './ValueDistribution';

const getLabeledValues = (filters: Filter[], plantings: NestedRowsQuery['plantings']) => {
  if (process.env.NODE_ENV !== 'production') {
    console.time(`Get labeled values`);
  }

  const plantingIdsOfFilters = filters.map((filter) => getPlantingIdsOfFilter(filter, plantings));

  // flat list of values from all plantings tagged with the matching filters
  const labeledValues = plantings
    .map((planting) => {
      const matchingFilters = filters
        .filter((_, idx) => plantingIdsOfFilters[idx].includes(planting.id))
        .map((filter) => ({ id: filter.id, color: filter.color }));
      return planting.values.map((value) => ({
        ...value,
        plantingId: planting.id,
        matchingFilters,
      }));
    })
    .flat();

  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`Get labeled values`);
  }

  return labeledValues;
};

const flattenRows = (
  rows: RowData[],
  labeledValues: ReturnType<typeof getLabeledValues>,
  nesting = 0,
): {
  row: RowData;
  nesting: number;
  childCount: number;
  isLastChild: boolean;
  hideBranches: number;
  childRowNames: string[];
  allData: {
    matchingFilters: {
      id: string;
      color: string | null;
    }[];
    name: string;
    value: number;
    modusTestId?: string | null;
    unit?: string | null;
    plantingId: string;
  }[];
}[] =>
  rows
    .map((row) => {
      const children = flattenRows(row.children || [], labeledValues, nesting + 1);
      const childCount = children ? findLastIndex(children, { nesting: nesting + 1 }) + 1 : 0;
      children.slice(childCount).forEach((child) => (child.hideBranches += 1));

      const childRowNames = children.map((c) => c.row.name);
      const { name, showAggregation } = row;
      const valueNames = showAggregation ? childRowNames : [name];
      const allData =
        childCount > 0
          ? []
          : labeledValues
              .filter((v) => valueNames.includes(v.name))
              .map((v) => ({ ...v, modusTestId: row.modusTestId, unit: row.unit }));

      if (allData.length === 0 && children.every((c) => c.allData.length === 0)) {
        return [];
      }

      // mark the last immediate child
      const lastChild = last(children.filter((c) => c.nesting === nesting + 1));
      if (lastChild) {
        lastChild.isLastChild = true;
      }

      return [
        {
          row,
          nesting,
          childCount,
          isLastChild: false,
          hideBranches: 0,
          childRowNames,
          allData,
        },
        ...children,
      ];
    })
    .flat();

interface Props {
  filters: Filter[];
  children?: never;
}

export const NestedRows: FunctionComponent<Props> = ({ filters }) => {
  const selectedCropType = useSelectedCropType();
  const { data, loading } = useNestedRowsQuery({
    variables: { cropType: selectedCropType },
    skip: !selectedCropType,
  });
  const { plantings, rows: normalizedRows } = data || {};
  const pinnedPlantingIds = usePinnedPlantingIds();
  const highlightedPlantingId = useHighlightedPlantingId();
  const highlightedFilterId = useHighlightedFilterId();

  const flatRows = useMemo(() => {
    const rows = covertNormalizedRows(normalizedRows?.filter(isNonNull) ?? []);
    const labeledValues = plantings ? getLabeledValues(filters, plantings) : [];
    return flattenRows(rows, labeledValues);
  }, [filters, normalizedRows, plantings]);

  const [isClosed, setIsClosed] = useState<{ [key: string]: boolean }>({});

  const getIsClosed = useCallback(
    (name: string) => {
      if (name in isClosed) {
        return isClosed[name];
      }
      // rows at the second level and below are closed by default
      return flatRows.find((r) => r.row.name === name)?.nesting === 1;
    },
    [flatRows, isClosed],
  );

  const toggleOpen = (name: string) => {
    setIsClosed({ ...isClosed, [name]: !getIsClosed(name) });
  };

  const openStates = useMemo(() => {
    let closedAtLevel = -1;
    return flatRows.map(({ row: { name }, nesting }) => {
      if (closedAtLevel < 0) {
        if (getIsClosed(name)) {
          closedAtLevel = nesting;
          return 'closed';
        } else {
          return 'open';
        }
      } else {
        if (closedAtLevel < nesting) {
          return 'parentClosed';
        } else if (getIsClosed(name)) {
          closedAtLevel = nesting;
          return 'closed';
        } else {
          closedAtLevel = -1;
          return 'open';
        }
      }
    });
  }, [flatRows, getIsClosed]);

  if (!loading && flatRows.length === 0) {
    return (
      <Alert severity="warning" sx={{ my: 3 }}>
        We found no data in the selected plantings.
      </Alert>
    );
  }

  return (
    <>
      {loading && (
        <Box mt={1}>
          <LinearProgress />
        </Box>
      )}
      {flatRows
        .map(
          (
            {
              row: { name, showAggregation },
              nesting,
              childCount,
              isLastChild,
              hideBranches,
              childRowNames,
              allData,
            },
            index,
          ) => (
            <ValueDistribution
              label={name}
              valueNames={showAggregation ? childRowNames : name}
              nesting={nesting}
              childCount={childCount}
              isLastChild={isLastChild}
              hideBranches={hideBranches}
              onToggleChildren={() => toggleOpen(name)}
              openState={openStates[index]}
              highlightedFilterId={highlightedFilterId || undefined}
              highlightedPlantingId={highlightedPlantingId || undefined}
              pinnedPlantingIds={pinnedPlantingIds}
              allData={allData}
            />
          ),
        )
        .map((node, index) =>
          index === 0 ? (
            <TourStop key={index} stop={StopEnum.HOVER_VALUE} placement="right">
              {node}
            </TourStop>
          ) : index === 1 ? (
            <TourStop key={index} stop={StopEnum.OPEN_PLANTING} placement="bottom">
              {node}
            </TourStop>
          ) : (
            <div key={index}>{node}</div>
          ),
        )}
    </>
  );
};

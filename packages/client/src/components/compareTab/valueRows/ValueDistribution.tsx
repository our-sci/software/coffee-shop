import React, { MouseEvent, useCallback, useEffect, useMemo, useState } from 'react';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useTheme, withTheme } from '@emotion/react';
import styled from '@emotion/styled';
import { extent, median, minIndex, quantile } from 'd3-array';
import { format } from 'd3-format';
import { scaleLinear } from 'd3-scale';
import { useCanvas } from 'hooks/useCanvas';
import groupBy from 'lodash/groupBy';
import range from 'lodash/range';
import sortBy from 'lodash/sortBy';
import { useHighlightPlanting, useUnhighlightPlanting } from 'states/highlightedPlantingId';
import { useAddPlantingCard, useShowPlantingCards } from 'states/sidePanelContent';
import tinycolor from 'tinycolor2';
import { ValuePopup } from './ValuePopup';

export const defaultTheme = {
  rowGap: 8,
  rowHeight: 20,
  tabSize: 6,
  branchWidth: 3,
  tickWidth: 2,
  medianTickWidth: 8,
  medianTickHeight: 6,
  varianceLineHeight: 2,
  labelWidth: 190,
  padding: 4,
  averageColor: tinycolor('white').setAlpha(0.1).toString(),
};

type OpenState = 'open' | 'closed' | 'parentClosed';

const Bar = withTheme(styled.div<{
  openState: OpenState;
}>`
  display: flex;
  position: relative;
  width: 100%;
  height: ${(p) =>
    p.openState === 'parentClosed'
      ? 0
      : p.theme.valueDistribution.rowHeight + p.theme.valueDistribution.rowGap}px;
  opacity: ${(p) => (p.openState === 'parentClosed' ? 0 : 1)};
  pointer-events: ${(p) => (p.openState === 'parentClosed' ? 'none' : 'auto')};
  transition: all 0.3s cubic-bezier(0.17, 0.42, 0.28, 0.99);
  transform: translateY(${(p) => (p.openState === 'parentClosed' ? -12 : 0)}px);
`);

const Label = withTheme(styled.div<{
  nesting: number;
  childCount: number;
  isHoveringLabel: boolean;
}>`
  flex: 0;
  position: relative;
  margin-top: ${(p) => p.theme.valueDistribution.rowGap / 2}px;
  margin-bottom: ${(p) => p.theme.valueDistribution.rowGap / 2}px;
  min-width: ${(p) =>
    p.theme.valueDistribution.labelWidth - p.nesting * p.theme.valueDistribution.tabSize}px;
  font-size: 13.5px;
  font-family: ${(p) => p.theme.font};
  font-weight: ${(p) => (p.childCount > 0 ? 700 : 400)};
  text-transform: uppercase;
  line-height: ${(p) => p.theme.valueDistribution.rowHeight}px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 0 4px;
  color: ${(p) => p.theme.colors.textPrimary};
  background-color: ${(p) => p.theme.colors.treeTitlePrimary};
  ${(p) =>
    p.childCount === 0
      ? `background: linear-gradient(to right, ${p.theme.colors.treeTitlePrimary}, ${p.theme.colors.treeTitleSecondary} 6px);`
      : ''}
  border-bottom-left-radius: ${(p) =>
    p.nesting === 0 ? p.theme.valueDistribution.branchWidth : 0}px;
  border-top-left-radius: ${(p) => (p.nesting === 0 ? p.theme.valueDistribution.branchWidth : 0)}px;
  transition: all 0.2s cubic-bezier(0.17, 0.42, 0.28, 0.99);
  cursor: pointer;
`);

const BranchLeft = withTheme(styled.div<{
  isEnd: boolean;
}>`
  width: ${(p) => p.theme.valueDistribution.branchWidth}px;
  height: 100%;
  margin-left: ${(p) =>
    p.theme.valueDistribution.tabSize - p.theme.valueDistribution.branchWidth}px;
  position: relative;
  top: ${(p) => -p.theme.valueDistribution.rowGap / 2}px;
  background-color: ${(p) => p.theme.colors.treeTitlePrimary};
  ${(p) =>
    p.isEnd ? `border-bottom-left-radius: ${p.theme.valueDistribution.branchWidth}px;` : ''}
`);

const BranchLeftHidden = withTheme(styled.div<{}>`
  width: ${(p) => p.theme.valueDistribution.tabSize}px;
`);

const Plot = withTheme(styled.div`
  flex: 1;
  min-width: 0px;
  position: relative;
  margin: ${(p) => p.theme.valueDistribution.rowGap / 2}px 0;
`);

const PlotCanvas = withTheme(styled.canvas`
  left: -${(p) => p.theme.valueDistribution.padding}px;
  top: -${(p) => p.theme.valueDistribution.padding}px;
  width: ${(p) => `calc(100% + ${p.theme.valueDistribution.padding * 2}px)`};
  height: ${(p) => `calc(100% + ${p.theme.valueDistribution.padding * 2}px)`};
  position: absolute;
`);

const LabelIcon = styled.div`
  display: flex;
  align-content: center;
  flex-direction: column;
  justify-content: center;
  height: 100%;
  position: absolute;
  top: 0;
  right: 0;
`;

type Props = {
  /**
   * Data name
   */
  label: string;
  valueNames: string[] | string;
  className?: string;
  nesting: number;
  childCount: number;
  isLastChild: boolean;
  hideBranches: number;
  onToggleChildren: () => void;
  openState: 'open' | 'closed' | 'parentClosed';
  highlightedFilterId?: string;
  highlightedPlantingId?: string;
  pinnedPlantingIds?: string[];
  allData: {
    matchingFilters: {
      id: string;
      color: string | null;
    }[];
    name: string;
    value: number;
    modusTestId?: string | null;
    unit?: string | null;
    plantingId: string;
  }[];
};

/**
 * Primary UI component for user interaction
 */
export const ValueDistribution = React.forwardRef<HTMLDivElement, Props>(
  (
    {
      label,
      valueNames,
      highlightedFilterId,
      highlightedPlantingId,
      pinnedPlantingIds = [],
      allData,
      ...rest
    },
    ref,
  ) => {
    valueNames = useMemo(
      () => (Array.isArray(valueNames) ? valueNames : [valueNames]),
      [valueNames],
    );
    const theme = useTheme();
    const canvas = useCanvas();
    const highlightPlanting = useHighlightPlanting();
    const unhighlightPlanting = useUnhighlightPlanting();
    const showPlantingCards = useShowPlantingCards();
    const addPlantingCard = useAddPlantingCard();

    const [isHoveringLabel, setIsHoveringLabel] = useState<boolean>(false);
    const [localHoveredValue, setLocalHoveredValue] = useState<{
      value: string;
      x: number;
      y: number;
      plantingId?: string;
    }>();

    const backgroundColor =
      rest.childCount === 0 ? theme.colors.treeBgSecondary : theme.colors.treeBgPrimary;
    const allValues = useMemo(() => allData.map((d) => d.value), [allData]);

    const scaleX = useMemo(() => {
      const [min, max] = extent(allValues);
      return scaleLinear()
        .domain([min || 0, max || 1])
        .rangeRound([
          theme.valueDistribution.padding + theme.valueDistribution.tickWidth / 2,
          canvas.width - theme.valueDistribution.padding - theme.valueDistribution.tickWidth / 2,
        ]);
    }, [
      allValues,
      canvas.width,
      theme.valueDistribution.padding,
      theme.valueDistribution.tickWidth,
    ]);

    const scaleY = useCallback(
      (val: number) => (val += theme.valueDistribution.padding),
      [theme.valueDistribution.padding],
    );

    const medianValue = useMemo(() => median(allValues) ?? 0, [allValues]);
    const [medianX, medianXMin, medianXMax, medianYMin, medianYMax] = useMemo(
      () => [
        scaleX(medianValue),
        scaleX(medianValue) - theme.valueDistribution.medianTickWidth / 2,
        scaleX(medianValue) + theme.valueDistribution.medianTickWidth / 2,
        scaleY(-theme.valueDistribution.medianTickHeight / 3),
        scaleY((theme.valueDistribution.medianTickHeight * 2) / 3),
      ],
      [
        medianValue,
        scaleX,
        scaleY,
        theme.valueDistribution.medianTickHeight,
        theme.valueDistribution.medianTickWidth,
      ],
    );

    const highlightIds = useMemo(() => {
      const ids = [...pinnedPlantingIds];
      if (highlightedPlantingId) {
        ids.push(highlightedPlantingId);
      }
      return ids;
    }, [highlightedPlantingId, pinnedPlantingIds]);

    const handlePlotMouseMove = (e: MouseEvent<HTMLCanvasElement>) => {
      const allSelectableValues = allData.map((d) => d.value);
      if (allSelectableValues.length === 0) {
        return;
      }

      const mouseX = e.nativeEvent.offsetX;
      const mouseY = e.nativeEvent.offsetY;

      const isMedian =
        mouseY >= medianYMin && mouseY < medianYMax && mouseX >= medianXMin && mouseX <= medianXMax;

      if (isMedian) {
        unhighlightPlanting();
        setLocalHoveredValue({
          value: `Median = ${Number(formatValue(medianValue))} ${allData[0]?.unit || ''}`.trim(),
          x: medianX,
          y: medianYMin,
        });
        return;
      }

      if (mouseY < theme.valueDistribution.varianceLineHeight) {
        unhighlightPlanting();
        return;
      }

      const targetValue = scaleX.invert(mouseX);
      const closestData = allData[minIndex(allSelectableValues, (v) => Math.abs(v - targetValue))];
      const closestValueX = scaleX(closestData.value);
      if (Math.abs(closestValueX - mouseX) <= theme.valueDistribution.tickWidth) {
        highlightPlanting(closestData.plantingId);
        setLocalHoveredValue({
          value: `${Number(formatValue(closestData.value))} ${closestData.unit || ''}`.trim(),
          x: closestValueX,
          y: scaleY(theme.valueDistribution.varianceLineHeight),
          plantingId: closestData.plantingId,
        });
      } else {
        unhighlightPlanting();
        setLocalHoveredValue(undefined);
      }
    };

    const handlePlotMouseLeave = () => {
      unhighlightPlanting();
      setLocalHoveredValue(undefined);
    };

    const handlePlotMouseClick = () => {
      if (localHoveredValue?.plantingId) {
        addPlantingCard(localHoveredValue.plantingId);
        showPlantingCards();
      }
    };

    // select maximum one filter for each value
    const allDataWithFilter = useMemo(
      () =>
        allData.map((value) => ({
          ...value,
          filter:
            value.matchingFilters.find((f) => f.id === highlightedFilterId) ??
            value.matchingFilters[0] ??
            null,
        })),
      [allData, highlightedFilterId],
    );

    useEffect(() => {
      const ctx = canvas.resize();
      if (!ctx) {
        return;
      }

      // move values to front (top) if they match the highlightedFilterId
      const valuesGroupedByFilter = sortBy(
        Object.values(groupBy(allDataWithFilter, (value) => value.filter?.id)),
        (valueSet) => valueSet[0].matchingFilters.some((f) => f.id === highlightedFilterId),
      );

      const tickHeight =
        canvas.height -
        theme.valueDistribution.padding * 2 -
        theme.valueDistribution.varianceLineHeight;

      for (const valueSet of valuesGroupedByFilter) {
        const { filter } = valueSet[0];
        ctx.beginPath();
        const color = tinycolor(filter?.color || theme.valueDistribution.averageColor);
        ctx.fillStyle = !highlightedFilterId
          ? color.toString()
          : highlightedFilterId === filter?.id
          ? color.saturate(2).toString()
          : color
              .desaturate(12)
              .setAlpha(color.getAlpha() / 2)
              .toString();

        // draw variance line
        if (highlightedFilterId && highlightedFilterId === filter?.id) {
          const values = valueSet.map((v) => v.value);
          const q1 = quantile(values, 0.25);
          const q3 = quantile(values, 0.75);

          if (q1 && q3) {
            // Draw variance line
            ctx.rect(
              scaleX(q1) - theme.valueDistribution.tickWidth / 2,
              scaleY(0),
              scaleX(q3) - scaleX(q1) - theme.valueDistribution.tickWidth / 2,
              theme.valueDistribution.varianceLineHeight,
            );
          }
        }

        // draw ticks
        valueSet.forEach((value) => {
          ctx.rect(
            scaleX(value.value) - theme.valueDistribution.tickWidth / 2,
            scaleY(theme.valueDistribution.varianceLineHeight),
            theme.valueDistribution.tickWidth,
            tickHeight,
          );
        });
        ctx.closePath();
        ctx.fill();
      }

      // Draw median
      if (allValues.length > 0) {
        ctx.beginPath();
        ctx.fillStyle =
          localHoveredValue && !localHoveredValue.plantingId
            ? theme.color('white')
            : theme.color('red');
        ctx.moveTo(medianXMin, medianYMin);
        ctx.lineTo(medianXMax, medianYMin);
        ctx.lineTo(medianX, medianYMax);
        ctx.closePath();
        ctx.fill();
      }

      // Draw hover
      if (highlightIds.length > 0) {
        ctx.fillStyle = theme.color('white');
        allDataWithFilter.forEach((data) => {
          if (highlightIds.includes(data.plantingId)) {
            ctx.fillRect(
              scaleX(data.value) - theme.valueDistribution.tickWidth / 2,
              scaleY(theme.valueDistribution.varianceLineHeight),
              theme.valueDistribution.tickWidth,
              tickHeight,
            );
          }
        });
      }
    }, [
      allDataWithFilter,
      allValues.length,
      canvas,
      highlightIds,
      highlightedFilterId,
      localHoveredValue,
      medianX,
      medianXMax,
      medianXMin,
      medianYMax,
      medianYMin,
      scaleX,
      scaleY,
      theme,
    ]);

    const leftBranches = rest.nesting - rest.hideBranches;

    const formatValue = useMemo(() => format('.3f'), []);

    return (
      <Bar ref={ref} className={rest.className} openState={rest.openState}>
        {range(rest.hideBranches).map((i) => (
          <BranchLeftHidden key={`blh-${i}`} />
        ))}
        {range(leftBranches).map((index) => (
          <BranchLeft key={index} isEnd={rest.isLastChild && index === leftBranches - 1} />
        ))}
        <Label
          nesting={rest.nesting || 0}
          childCount={rest.childCount || 0}
          onClick={() => rest.onToggleChildren()}
          onMouseEnter={() => setIsHoveringLabel(true)}
          onMouseLeave={() => setIsHoveringLabel(false)}
          isHoveringLabel={isHoveringLabel && rest.openState === 'closed'}
        >
          {label}
          {rest.childCount > 0 && (
            <LabelIcon>
              {rest.openState === 'open' ? (
                <ExpandLessIcon fontSize="inherit" sx={{ opacity: isHoveringLabel ? 1 : 0 }} />
              ) : (
                <ExpandMoreIcon fontSize="inherit" sx={{ opacity: isHoveringLabel ? 1 : 0.5 }} />
              )}
            </LabelIcon>
          )}
        </Label>
        <Plot style={{ backgroundColor }}>
          <PlotCanvas
            onMouseMove={handlePlotMouseMove}
            onMouseLeave={handlePlotMouseLeave}
            onClick={handlePlotMouseClick}
            ref={canvas.ref}
          />

          {localHoveredValue && <ValuePopup {...localHoveredValue} />}
        </Plot>
      </Bar>
    );
  },
);

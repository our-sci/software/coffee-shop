import React, { forwardRef, MouseEvent, useMemo } from 'react';
import { useState } from 'react';
import AddIcon from '@mui/icons-material/Add';
import Button from '@mui/material/Button';
import ListItemText from '@mui/material/ListItemText';
import MenuView, { MenuProps } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import styled from '@mui/material/styles/styled';
import { SurveyStackGroup } from 'graphql/resolvers.generated';
import { Menu } from 'models/Menu';
import { useAuth } from 'states/auth';
import { useAddFilter } from 'states/filters';
import { useShowFilterEditor } from 'states/sidePanelContent';
import { isNonNil } from 'utils/validate';
import { useAddFilterButtonQuery } from './AddFilterButton.generated';
import { DropMenuItem } from './DropMenuItem';

const StyledMenu = styled(MenuView)<MenuProps>({
  '& .MuiList-root': {
    maxWidth: '450px',
    maxHeight: 'calc(100% - 120px)',
  },
  '& .MuiList-root .MuiCollapse-wrapper .MuiListItemText-primary': {
    fontSize: '0.875rem',
    lineHeight: '24px',
  },
  '& .MuiList-root .MuiCollapse-wrapper .MuiMenuItem-root': {
    paddingTop: '0px',
    paddingBottom: '0px',
    height: '40px',
  },
});

interface Props {
  children?: never;
}

const getRootGroups = (groups: SurveyStackGroup[]): SurveyStackGroup[] =>
  groups.filter(({ dir }) => !groups.some(({ path }) => path === dir));

const getMenuTree = (groups: SurveyStackGroup[], parent?: SurveyStackGroup): Menu[] => {
  const rootGroups = getRootGroups(groups);
  const childGroups = parent ? groups.filter((group) => group.dir === parent.path) : rootGroups;

  return childGroups
    .filter(
      (group) =>
        // has access itself
        group.coffeeShopSettings?.groupHasCoffeeshopAccess ||
        // one of the descendant has access
        groups
          .filter((g) => g.dir.startsWith(group.path))
          .some((group) => group.coffeeShopSettings?.groupHasCoffeeshopAccess),
    )
    .map((group) => {
      const children = getMenuTree(groups, group);
      return {
        id: group.id,
        name: group.name,
        children,
        disabled: !group.coffeeShopSettings?.groupHasCoffeeshopAccess,
      };
    });
};

export const AddFilterButton = forwardRef<HTMLButtonElement, Props>((_, ref) => {
  const auth = useAuth();
  const addFilter = useAddFilter();
  const showFilterEditor = useShowFilterEditor();

  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

  const open = Boolean(anchorEl);

  const { myFarms, surveyStackGroups } =
    useAddFilterButtonQuery({
      variables: { userId: (auth.isAuthenticated && auth.user.id) || null },
    })?.data || {};

  const farm = useMemo<Menu | null>(() => {
    const children =
      myFarms?.filter(isNonNil).map((farm) => ({
        id: farm.id,
        name: farm.id,
        children: [],
      })) ?? [];

    if (children?.length === 0) {
      return null;
    }

    return { id: '', name: 'My Farms', children };
  }, [myFarms]);

  const organization = useMemo<Menu | null>(() => {
    const children = getMenuTree(surveyStackGroups ?? []);

    if (children.length === 0) {
      return null;
    }

    return { id: '', name: 'My Organizations', children };
  }, [surveyStackGroups]);

  const handleOpen = (e: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleNew = () => {
    handleClose();
    const filterId = addFilter();
    showFilterEditor(filterId);
  };

  return (
    <>
      <Button
        ref={ref}
        size="medium"
        id="basic-button"
        startIcon={<AddIcon />}
        onClick={handleOpen}
      >
        Add
      </Button>
      <StyledMenu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{ 'aria-labelledby': 'basic-button' }}
      >
        <MenuItem onClick={handleNew}>
          <ListItemText>New Filter</ListItemText>
        </MenuItem>
        {farm && <DropMenuItem item={farm} type="farmDomain" />}
        {organization && <DropMenuItem item={organization} type="organization" />}
      </StyledMenu>
    </>
  );
});

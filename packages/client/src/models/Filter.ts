export enum FilterParamDataSource {
  FarmOnboarding,
  Values,
}

export interface FilterValueOption {
  options: string[];
}

export interface FilterValueRange {
  max: number;
  min: number;
}

export type FilterValue = FilterValueOption | FilterValueRange;

export interface Filter {
  color: string;
  id: string;
  name: string;
  params: FilterParam[];
}

export interface FilterParam {
  active: boolean;
  dataSource: FilterParamDataSource;
  key: string;
  value: FilterValue;
}

export interface FilterableNumeric {
  type: 'numeric';
  key: string;
  modusId?: string;
  values: number[];
  dataSource: FilterParamDataSource;
}

export interface FilterableOption {
  type: 'option';
  key: string;
  options: {
    value: string;
    occurrences: number;
  }[];
  dataSource: FilterParamDataSource;
}

export type Filterable = FilterableNumeric | FilterableOption;

export const isOptionFilterParam = (
  param: FilterParam,
): param is FilterParam & { value: FilterValueOption } => {
  return 'options' in param.value;
};

export const isRangeFilterParam = (
  param: FilterParam,
): param is FilterParam & { value: FilterValueRange } => {
  return 'min' in param.value;
};

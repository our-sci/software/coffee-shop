export enum NotificationTypeEnum {
  ERROR,
}

export interface Notification {
  id: string;
  type: NotificationTypeEnum;
  message: string;
}

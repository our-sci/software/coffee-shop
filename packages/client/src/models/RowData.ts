export interface RowData {
  name: string;
  type: string;
  children?: RowData[];
  showAggregation?: boolean;
  unit?: string | null;
  modusTestId?: string | null;
}

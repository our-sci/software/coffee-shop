export interface Planting {
  id: string;
  organization?: string[] | null;
  producer?: { id: string } | null;
  values: {
    name: string;
    value: number;
  }[];
  farmOnboarding?: {
    values?:
      | {
          key: string;
          values: string[];
        }[]
      | null;
  } | null;
}

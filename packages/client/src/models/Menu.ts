export interface Menu {
  id: string;
  name: string;
  disabled?: boolean;
  children: Menu[];
}
